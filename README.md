# <img src="https://gitlab.tudelft.nl/aj-lab/teaching/raw/master/binder/images/logo.jpg" width="250"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Teaching environment of the [AJ lab](http://cryoem.tudelft.nl) at [TU Delft](http://tudelft.nl)

## [NB2071 – Physical Biology of the Cell 2](https://gitlab.tudelft.nl/aj-lab/teaching/wikis/NB2071)  

#### Course description  
Biological cells are complex biochemical systems that obey the laws of physics. Modern research in molecular and cell biology therefore increasingly relies on physical concepts. Quantitative measurements, and models to design and interpret them, are essential as we build our molecular understanding of cellular phenomena such as the regulation of cytoskeletal polymerisation, replication of nucleic acids, intracellular transport and translocation across membranes to name but a few of many examples.

In this course you will (1) learn how to connect concepts from physics and physical chemistry with observations from biology and biochemistry to quantitatively model this diversity of cellular processes and to understand their molecular basis. You will (2) learn about thermodynamics and kinetics of biomolecular interactions and reactions, the structure, assembly, dynamics and evolution of protein complexes and the principles of self-assembly and self-organisation in biological cells. You will then (3) apply these concepts to concrete examples in which you will establish quantitative models for the regulation of biochemical cascades and circuits, the functional dynamics of molecular machines and processes related to translocation and the propagation and processing of biological signals. 

#### You can find the practicals [here](https://gitlab.tudelft.nl/aj-lab/teaching/wikis/NB2071).

## [NB3012 – Protein structure - Theory and Tools](https://gitlab.tudelft.nl/aj-lab/teaching/wikis/NB3012)  

## [NB4020 – High-Resolution Imaging](https://gitlab.tudelft.nl/aj-lab/teaching/wikis/NB4020) 

#### Course description  
Microscopy is an essential technique in the life science and developments in microscopy have almost always led to new biological discoveries. In the course ‘High Resolution Imaging’ we introduce the state of the art techniques to observe cells, tissue, and biological molecules at the highest possible resolution. We provide a working knowledge of various high resolution imaging techniques, present the basic principles and limitations behind these techniques, and also discuss how to prepare biological samples for the different forms of microscopy. Examples from recent scientific literature will be used to illustrate what can be and has been achieved. Targeted homework, lab demonstrations, and experimental as well as theoretical exercises will help to familiarize you with the different types of microscopes and the data they provide.
Specifically, the course includes lectures and demos on:

• Superresolution fluorescence microscopy  
• Scanning electron microscopy (SEM), Transmission electron microscopy (TEM), and scanning transmission electron microscopy (STEM)  
• Electron cryo-microscopy, and methods to calculate 3D structures from 2D projections  
• Correlative light and electron microscopy and techniques for large-scale and volume imaging  

#### You can find the practicals [here](https://gitlab.tudelft.nl/aj-lab/teaching/wikis/NB4020).


<small>&copy; Copyright 2018-2020, Arjen J. Jakobi <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><b>CC-BY-NC-SA</b></a></small>  
<small>The material in this repository is distributed as Open Educational Resource (OER) under a [CC-BY-NC-ND](http://creativecommons.org/licenses/by-nc-nd/4.0/) licence.  
You may freely copy, reuse, adapt and distribute the material in any form compliant with this licence.</small>

