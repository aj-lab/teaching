def view_adk_traj():
    import MDAnalysis as mda
    import nglview as nv
    import warnings
    warnings.filterwarnings('ignore')
    from MDAnalysis.tests.datafiles import PSF, DCD
    # Define trajectory
    adk_trajectory = mda.Universe(PSF, DCD)
    protein = adk_trajectory.select_atoms("protein")

    # View trajectory
    adk_trajectory.add_TopologyAttr('altLocs')
    adk_trajectory.add_TopologyAttr('icodes')
    adk_trajectory.add_TopologyAttr('occupancies')
    adk_trajectory.add_TopologyAttr('tempfactors')
    view = nv.show_mdanalysis(protein)
    view.clear_representations()

    view.add_cartoon('(not 30-63 and not 118-167)', color='gray')
    view.add_cartoon('(30-63)', color='cyan')
    view.add_cartoon('(118-167)', color='magenta')
    return view

def view_co_traj():
    import nglview as nv
    import MDAnalysis as mda
    traj = mda.Universe("trajectories/CO_traj.pdb", "trajectories/CO_traj.dcd")
    view = nv.show_mdanalysis(traj)
    #view.clear_representations()
    view.add_spacefill(radius_type='vdw',radiusScale=1.0)
    view.control._view_xz_plane()
    #view.player.parameters = dict(delay=0.00005, step=1)
    return view

def plot_traj(distances, energies, combine="False"):
    from matplotlib import pyplot as plt    
    if combine:
        fig, ax1 = plt.subplots()
        color = ['tab:blue','tab:red']
        ax1.set_xlabel('Simulation time [ps]', fontsize=18)
        ax1.set_ylabel('C--O distance [nm]', color=color[0], fontsize=18)
        ax1.plot(distances, color=color[0])
        ax1.tick_params(axis='y', labelcolor=color[0])
        ax2 = ax1.twinx()  
        ax2.set_ylabel('Energy [kJ/mol]', color=color[1], fontsize=18)  
        ax2.plot(energies, color=color[1])
        ax2.tick_params(axis='y', labelcolor=color[1])
        ###
        fig.tight_layout()
        return fig
	#return ax1, ax2
    else:     
        fig, axs = plt.subplots(2,1,figsize=(6.,8.))
        color = ['tab:blue','tab:red']
        ax1 = axs[0]
        ax1.plot(distances, color=color[0])
        ax1.set_ylabel('C--O distance [nm]', fontsize=18);
        ax2 = axs[1]
        ax2.plot(energies, color=color[1])
        ax2.set_xlabel('Simulation time [ps]', fontsize=18)
        ax2.xaxis.labelpad = 10
        ax2.set_ylabel('Energy [kJ/mol]', fontsize=18)
        fig.align_ylabels(axs[:])
        return fig
	# return axs

def plot_pair_distance_distribution_function():
    import numpy as np
    #from numpy import genfromtxt
    from matplotlib import pyplot as plt
    saxs_open = np.genfromtxt('data/saxs/4ake.dat', delimiter=',')
    saxs_closed = np.genfromtxt('data/saxs/1ake.dat', delimiter=',')
    r_open = np.array([i[0] for i in saxs_open])
    p_r_open = np.asarray([i[1] for i in saxs_open])
    r_closed = np.array([i[0] for i in saxs_closed])
    p_r_closed = np.asarray([i[1] for i in saxs_closed])
    color = ['tab:blue','tab:red']
    fig, ax1 = plt.subplots()
    ax1.plot(r_open,p_r_open, color=color[0])
    ax1.set_yticklabels([])
    ax1.set_xlabel('r [Ångstrom]', fontsize=18)
    ax1.set_ylabel('P(r) [a.u.]', fontsize=18);
    ax2 = ax1.twinx()
    ax2.plot(r_closed,p_r_closed, color=color[1])
    ax2.set_yticklabels([])
    fig.tight_layout()
    return ax1, ax2

def import_saxs_data():
    import numpy as np
    saxs_open = np.genfromtxt('data/saxs/4ake.dat', delimiter=',')
    saxs_closed = np.genfromtxt('data/saxs/1ake.dat', delimiter=',')
    r_open = np.array([i[0] for i in saxs_open])
    p_r_open = np.asarray([i[1] for i in saxs_open])
    r_closed = np.array([i[0] for i in saxs_closed])
    p_r_closed = np.asarray([i[1] for i in saxs_closed])
    pddf_open = [r_open, p_r_open]
    pddf_closed = [r_closed, p_r_closed]
    return pddf_open, pddf_closed

def compute_radius_of_gyration(trajectory=None):
    import numpy as np
    import matplotlib.pyplot as plt
    import MDAnalysis as mda
    from MDAnalysis.tests.datafiles import PSF, DCD
    # Define trajectory
    trajectory = mda.Universe(PSF, DCD)
    radius_of_gyration = []
    protein = trajectory.select_atoms("protein")
    for ts in trajectory.trajectory:
        radius_of_gyration.append((trajectory.trajectory.time, protein.radius_of_gyration()))
    radius_of_gyration = np.array(radius_of_gyration)
    # plot Rg
    ax = plt.subplot(111)
    ax.plot(radius_of_gyration[:,0], radius_of_gyration[:,1], 'ro', markersize=3, label=r"$R_G$")
    ax.set_xlabel("time (ns)", fontsize=16)
    ax.set_ylabel(r"radius of gyration $R_G$ ($\AA$)", fontsize=16)
    #ax.figure.savefig("radius_of_gyration.pdf")
    #plt.draw()
    return ax
