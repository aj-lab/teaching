def init_data():
    import numpy as np
    conc_I   = np.asarray([0.000, 0.25, 0.50, 0.75, 1.00, 1.25, 1.50, 1.75, 2.00, 4.00, 12.00, 20.00])
    theta_m1 = np.asarray([0.000, 0.200, 0.333, 0.429, 0.500, 0.556, 0.600, 0.636, 0.667, 0.800, 0.923, 0.952])
    theta_m2 = np.asarray([0.000, 0.400, 0.667, 0.857, 1.000, 1.111, 1.200, 1.273, 1.333, 1.500, 1.846, 1.905])
    return conc_I, theta_m1, theta_m2

def init_data_lac():
    import numpy as np
    c_iptg = np.asarray([-6.1, -5.7, -5.56, -5.5, -5.25, -5.1, -4.8, -4.6, -4.4, -4.25])
    iptg_theta = np.asarray([0.125, 0.27, 0.37, 0.48, 0.515, 0.68, 0.75, 0.85, 0.9, 0.95])
    co_iptg = np.asarray([-5.1, -4.75, -4.45, -4.3, -4.15, -4.05, -4.00, -3.8, -3.5])
    o_iptg_theta = np.asarray([0.05, 0.125, 0.22, 0.37, 0.42, 0.5, 0.61, 0.7, 0.9])
    return c_iptg, iptg_theta, co_iptg, o_iptg_theta

def plot_lb(a,b,c, hill="False"):
    import numpy as np
    from matplotlib import pyplot as plt    
    al = np.log((a[1:11]))
    hb = np.log((b[1:11]/(1-b[1:11])))
    if hill:
        fig, ax1 = plt.subplots()
        color = ['tab:blue','tab:red']
        ax1.set_xlabel('log[O]', fontsize=18)
        ax1.set_ylabel('log(θ/1-θ)', color=color[0], fontsize=18)
        ax1.plot(al,hb, color=color[0], marker='o', linestyle=':')
        #ax1.plot(a,hc, color=color[1])
        fig.tight_layout()
        leg = ax1.legend();
        return fig
    else:     
        fig, ax1 = plt.subplots()
        color = ['tab:blue','tab:red']
        ax1.set_xlabel('[O] mM', fontsize=18)
        ax1.set_ylabel('θ', fontsize=18)
        ax1.plot(a,b, color=color[0], marker='o', linestyle=':' , label="Method 1")
        ax1.plot(a,c, color=color[1], marker='o', linestyle=':', label="Method 2")
        fig.tight_layout()
        leg = ax1.legend();
        return fig
    
def plot_lac(a,b, label, hill=False):
    import numpy as np
    from matplotlib import pyplot as plt    
    a = 10**a
    al = np.log((a[1:11]))
    hb = np.log((b[1:11]/(1-b[1:11])))
    fit = np.polyfit(al,hb,1)
    fit_fn = np.poly1d(fit)
    if hill:
        print('Hill coefficient:', fit[0])
        fig, ax1 = plt.subplots()
        color = ['tab:blue','tab:red']
        ax1.set_xlabel('log[IPTG]', fontsize=18)
        ax1.set_ylabel('log(θ/1-θ)', color=color[0], fontsize=18)
        #ax1.plot(al,hb, color=color[0], marker='o', linestyle=':', label=label)
        ax1.plot(al,hb, 'bo', al, fit_fn(al), '--k', label=label)
        #ax1.plot(a,hc, color=color[1])
        fig.tight_layout()
        leg = ax1.legend();
        return fig
    else:     
        fig, ax1 = plt.subplots()
        color = ['tab:blue','tab:red']
        ax1.set_xlabel('[IPTG] mM', fontsize=18)
        ax1.set_ylabel('θ', fontsize=18)
        ax1.plot(a,b, color=color[0], marker='o', linestyle=':', label=label)
        fig.tight_layout()
        leg = ax1.legend();
        return fig
