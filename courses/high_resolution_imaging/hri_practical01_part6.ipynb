{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font color='DarkBlue'>High-Resolution Imaging (NB4020) \n",
    "----\n",
    "## <font color='CornflowerBlue'>Practical 1: Introduction to Fourier Optics and Image Processing\n",
    "    \n",
    "## The convolution theorem\n",
    " \n",
    "### What again is convolution?\n",
    "As we have already discussed in the lecture, convolution is a mathematical process to describe how one function is modified by another function. The convolution equation is given by the following relationship. \n",
    "\\begin{equation}\n",
    "    (f \\circledast g) (y) = \\int_{-\\infty}^{\\infty} f(x)g(y-x)dx\n",
    "\\end{equation}\n",
    "where $f$ and $g$ are two functions and $\\circledast$ is the convolution operator. \n",
    "\n",
    "Take a look at these following examples of 1D convolution:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from shared.misc import play_with_convolution_illustration\n",
    "play_with_convolution_illustration();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As illustrated above, to convolve two functions the we have to move one function relative to the other by sweeping every possible location from $-\\infty$ to $\\infty$. The result of the convolution is a new function that is the product of the two functions. In the illustration above, we move the function $g(x)$ relative to $f(x)$. The two functions are multiplied at each point and the sum of the result is the height of the red dot. The x coordinate of the red dot shows the instantaneous location of the function $g(x)$.  \n",
    "    \n",
    "Convolution is an important concept in image processing because it gives us a way to characterize the effects of an imaging system on the final image (i.e. determining its transer function). In the next section, we shall see some of the most useful applications of convolution for images. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Convolution in 2D \n",
    "\n",
    "Convolution in 2D is especially useful for many image processing applications such as edge detection and blurring. These are accomplished using kernels, which are matrices used to convolve an image with. Here are some fun interactives which demonstrate the use of kernels\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b>Kernels</b>\n",
    "\n",
    "Run the following code, and choose different kernels, and sizes, and see their effect on the input image. The following kernels are coded in this interactive: \n",
    "> 1. Sobel: This kernel is used to detect edges.  \n",
    "> 2. Sharpen: This kernel is used to sharpen the image \n",
    "> 3. Box blur: This performs average operations over the whole kernel to blur an image \n",
    "> 4. Gaussian blur: This kernel is obtained from a discrete Gaussian function to smoothly blur the edge of an image "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from shared.misc import *\n",
    "I = get_image('cameraman',256)\n",
    "plt.imshow(I,cmap='gray');"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "play_with_image_kernels(I)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can create your own kernel! Run the following code, to enter your custom kernel and the code following that to apply this custom kernel to the image. \n",
    "\n",
    "Try changing the terms below, and see the effect of your custom kernel on the image. See if you can derive a new kernel for edge detection, sharpening, or simply to do any other fun effects on your image! \n",
    "\n",
    "Change the parameter 'size' if you'd like to have a bigger kernel. The default value is set at 3. Try to use an odd number for your kernel as it's generally the case. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "size = 5\n",
    "sheet1 = from_array(np.zeros((size,size)))\n",
    "sheet1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "extract_kernel_and_convolve(I,sheet1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " ### Convolution Theorem\n",
    " \n",
    "The convolution theorem is one of the most important relationships in Fourier theory, and in its application to digital image processing. It can be stated as follows:\n",
    "\n",
    "---\n",
    "Consider functions a and b. Let A be the Fourier transform of a, and B be the Fourier transform of b. Then the Fourier transform of the product ab is the convolution of A and B. Call the result of this convolution C, then: \n",
    "\n",
    "<a id='equation_10'></a>\n",
    "$\n",
    "\\begin{equation}\n",
    "    C(h) = A(h)B(h)  \\tag {10}\\\n",
    "\\end{equation}\n",
    "$\n",
    "\n",
    "and \n",
    "\n",
    "<a id='equation_11'></a>\n",
    "$\n",
    "\\begin{equation}\n",
    "    c(x) = \\sum_{y} a(y)b(x-y) = \\mathcal{F}^{-1}\\{C(h\\}  \\tag {11}\\\n",
    "\\end{equation}\n",
    "$\n",
    "\n",
    "The product of two functions is simple - the values of the two functions are simply multiplied at every point.\n",
    "\n",
    "The convolution of two functions is more complex. To convolve two functions, the first function must be superimposed on the second at every possible position, and multiplied by the value of the second function at that point. The convolution is the sum of all of these superpositions. The convolution theorem therefore allows us to perform such calculation much faster (in Fourier space) than the convolution itself (in Real space).\n",
    "\n",
    "---\n",
    "\n",
    "In rigorous terms, suppose $f(x,y)$ and $g(x,y)$ are two real-valued functions varying along $x$ and $y$ axes. Let $\\tilde{F}(u,v)$ and $\\tilde{G}(u,v)$ be the fourier transforms of $f(x,y)$ and $g(x,y)$ respectively, with $u$ and $v$ being the coordinates in frequency space. The convolution theorem states that: \n",
    " \n",
    "<a id='equation_12'></a>\n",
    "$\n",
    "\\begin{equation}\n",
    "     f(x,y) * g(x,y) =  \\mathcal{F}^{-1} \\{\\tilde{F}(u,v) \\cdot \\tilde{G}(u,v)\\} \\tag {12}\\\n",
    "\\end{equation}\n",
    "$\n",
    "\n",
    "where, \n",
    "\n",
    "$*$ refers to convolution and $\\mathcal{F}^{-1}$ represents the inverse Fourier Transform operation.\n",
    "\n",
    "\n",
    "To implement this practically to our Kernel examples above, we first need to compute the Fourier transforms of our input image and the kernel. These form the two functions $f$ and $g$. However, since the kernels we used before had a different size than our image, we also need to padd the kernel with zeros around it so that the sizes become equal. We then form the product of their Fourier transforms. Finally, we take the inverse Fourier transform of this product obtain the result of the original convolution. \n",
    "\n",
    "Try the following interactive to demonstrate the convolution theorem. The results appear in the following format: \n",
    "\n",
    "\n",
    ">[input image] >> [padded kernel] >> <i>Real Space</i> \n",
    "\n",
    ">[Fourier Transform of input image] >> [Fourier Transform of padded kernel] >> <i>Fourier Space</i> \n",
    "\n",
    "> [Final convolution] >>  >> <i>Real Space</i> \n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "convolution_theorem(I)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now compare the timings required to run both these algorithm. The images in the bottom appear in the following format:\n",
    "\n",
    ">[input image] $\\>$$\\>$ [Convolution using FFT algorithm] \n",
    "\n",
    ">[input image] $\\>$$\\>$ [Convolution using convolve2D algorithm] \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "compare_fft_convolve(I)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You will find that the FFT algorithm is much faster when the kernel size selected is larger. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Prev: Convolutions](hri_practical01_part5.ipynb)    \n",
    "[Next: CTF](hri_practical01_part7.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.10.4 ('teaching')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  },
  "vscode": {
   "interpreter": {
    "hash": "6339fb7d6b5790b70c356b6b3f2583ce36f2ae1fff9d286f274a2877217e9957"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
