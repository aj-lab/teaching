# -*- coding: utf-8 -*-
"""
Created on Mon Sep  7 16:23:05 2020

@author: abharadwaj1
"""
############################################################################################################

import numpy as np
from numpy import sin,pi
import matplotlib.pyplot as plt
import matplotlib.image as image
from ipywidgets import interact, interactive, fixed, interact_manual, FloatSlider, IntSlider
import ipywidgets as widgets
from scipy.interpolate import interp1d
import math
from skimage.color import rgb2gray
import pandas as pd
import cmath
import matplotlib.patches as patches
from colorsys import hsv_to_rgb
from math import sin,cos,sqrt,atan2
from scipy import ndimage
from time import time
from scipy.signal import convolve2d
import shared.filters as filters
from ipysheet import sheet, from_array, to_array
from ipysheet.utils import extract_data
from skimage.transform import resize
import numexpr as ne
import mrcfile
pi = np.pi # global pi
############################################################################################################

############################################################################################################
# Part 0: common functions
############################################################################################################

def get_image(name,size):
    if name=='cameraman':
        im_in = rgb2gray(plt.imread('data/cameraman.bmp'))
        im_out = resize(im_in,(size,size))
    
    if name=='thankyou':
        im_in = rgb2gray(plt.imread('data/thankyou.jpg'))
        im_out = resize(im_in,(size,size))
    
    if name == "cryo_em_image":
        mrc_in = np.flip(mrcfile.open("data/6nbcmicrograph_0_c0_-2502.mrc").data, axis=0)[0]
        # crop the central 1024x1024 pixels from original image
        mrc_in = mrc_in[512:1536, 512:1536]
        #im_in = rgb2gray(mrc_in)
        im_out = resize(mrc_in,(size,size))
    
    if name == "spike_protein":
        mrc_in = np.flip(mrcfile.open("data/pdb_6crv_removed_glycan_atoms_partialmicrograph_0_c0_-50000.mrc").data, axis=0)[0]
        # crop the central 1024x1024 pixels from original image
        mrc_in = mrc_in[512:1536, 512:1536]
        #im_in = rgb2gray(mrc_in)
        im_out = resize(mrc_in,(size,size))
    return im_out


def show_these(allplots,fsize=12):
    f = plt.figure(figsize=(fsize,fsize))
    n = len(allplots)
    k = 1
    num_cols = 2
    num_rows = n // 2 + 1 
    plt.subplots_adjust(wspace=1/n,hspace=1/n)
    for im in allplots:
        subplot_index = num_rows*100+num_cols*10+k
        ax = f.add_subplot(subplot_index)
        ax.imshow(im,cmap='gray')
        k += 1
        
    
############################################################################################################
# Part 1: What is a wave
############################################################################################################

def plotwave(Amplitude,Frequency,Phase=0):
    times = np.linspace(0,5,1000)
    f = Amplitude * np.cos(2*pi*Frequency*times - Phase)
    plt.plot(times,f)
    plt.xlabel('X axis ')
    plt.ylabel('Function, $f(x)$')
    plt.ylim([-5,5])
    return 
def play_with_wave():
    return interact(plotwave,Amplitude=IntSlider(description="Amplitude",value=2,min=-4,max=4),Frequency=IntSlider(description="Freq ",value=4,min=1,max=8),Phase=FloatSlider(description="Phase (rad)",value=0.0,min=-2*pi,max=2*pi,step=0.5),continuous_update=False)

############################################################################################################
# Part 2: Reconstructing waves
############################################################################################################
def plotbox(width):
    N = 1000
    index = int(N/2)
    halfbox = int(width/2)
    signal = np.zeros(N)
    signal[index-halfbox:index+halfbox+1] = 1
    xaxis = np.linspace(-N/2,N/2,N)
    plt.plot(xaxis,signal)
    plt.xlabel('x axis')
    plt.ylabel('$b(x)$')
    plt.ylim([-0.5,1.5])
    return
def play_with_box_wave():    
    return interact(plotbox,width=IntSlider(description='Width (pixels)',value=100,min=50,max=500,step=50),continuous_update=False)

def getbox(width,N=1000,index=500,high=1,low=0):
    ''' returns a box function of lenght N and box size a 
        Note: signal[mid-halfbox] = high
              signal[mid+halfbox] = low
    '''
    signal = np.zeros(N)+low
    halfbox = int(width/2)
    signal[index-halfbox:index+halfbox+1] = high
    return signal

def getstep(index,N=1000,high=1,low=0):
    ''' returns a step function of lenght 1000 and box size a 
        Note: signal[mid-halfbox] = high
              signal[mid+halfbox] = low
    '''
    signal = np.zeros(N) + low
    signal[int(N/2)+index:] = high
    return signal
def play_with_reconstruction(Input):
    return interact(reconstruct,InputSignal=fixed(Input),tot_freq=IntSlider(description="# waves",value=50,min=1,max=100,step=1),return_label=fixed(False))

def reconstruct(InputSignal,tot_freq,return_label=False):
    a = [] #for cosing terms
    b = [] #for sin terms
    Amp = []
    Ph = []
    N = len(InputSignal) #This is the 'period' of the input function. 
    for freq in range(tot_freq):
        cosintegral = 0
        sinintegral = 0
        for index in range(N):
            cosintegral += InputSignal[index] * cos(index*2*np.pi*freq/N)
            sinintegral += InputSignal[index] * sin(index*2*np.pi*freq/N)
        a.append(2/N*cosintegral)
        b.append(2/N*sinintegral)
        Amp.append(sqrt(a[freq]**2+b[freq]**2))
        Ph.append(atan2(b[freq],a[freq]))

    recon = np.zeros(N)
    xaxis = np.linspace(-N/2,N/2,N)
    for freq in range(tot_freq):
        amp = Amp[freq]
        phase = Ph[freq]
        recon += amp * np.cos(2*np.pi*freq*xaxis - phase) 
    recon += -Amp[0]/2
    amplitudes = np.array(Amp)
    phases = np.array(Ph)
    if return_label:
        return recon,Amp,Ph
    else:
        plt.plot(xaxis,InputSignal,xaxis,recon)
        plt.xlabel('X axis (pixels)')
        plt.ylabel('Amplitude')
        plt.title('Reconstructed using sinusoidal waves')
def plot_random_lines(p=50,N=1000):
    random_input = np.random.rand(p)
    f = interp1d(range(p),random_input,kind='cubic')
    xnew = np.linspace(0,p-1,N)
    newinput = f(xnew)
    plt.plot(newinput)
    return newinput

def getwave(amp,freq,phase,length=1000):
    xaxis = np.linspace(-length/2,length/2,length)
    wave = np.zeros(length)+0.5
    wave += amp * np.cos(2*np.pi*freq*xaxis - phase)
    
    return wave

from mpl_toolkits.mplot3d import Axes3D

def check_first_few_waves(Input):
    return interact_manual(interactive_reconstruction,InputSignal=fixed(Input),F=IntSlider(description="# waves",value=3,min=1,max=15,step=1))

def interactive_reconstruction(InputSignal,F):
    F=F+1
    out = reconstruct(InputSignal,F,True)
    reconstructed = out[0]
    amplitudes = out[1]
    phases = out[2]
    frequencies = np.arange(F)
    length = len(reconstructed)
    xaxis = np.linspace(-length/2,length/2,length)
    ones = np.ones(1000,dtype=int)
    fig = plt.figure()
    ax = Axes3D(fig)

    for index in range(F+1):
        if index == 0:
            yaxis = ones*(-1)
            ax.plot(xaxis,yaxis,InputSignal)
            ax.set_xlabel('X axis')
            ax.set_ylabel('Spectral Order')
            ax.set_zlabel('Amplitude')
        elif index == 1:
            yaxis = ones*(0)
            ax.plot(xaxis,yaxis,reconstructed)
            ax.set_xlabel('X axis')
            ax.set_ylabel('Spectral Order')
            ax.set_zlabel('Amplitude')            
        else:
            wave = getwave(amplitudes[index-1],frequencies[index-1],phases[index-1])
            yaxis = ones*(index-1)
            ax.plot(xaxis,yaxis,wave)
            ax.set_xlabel('X axis')
            ax.set_ylabel('Spectral Order')
            ax.set_zlabel('Amplitude')


def get_random_line(p,N=1000):
    random_input = np.random.rand(p)
    f = interp1d(range(p),random_input)
    xnew = np.linspace(0,p-1,N)
    newinput = f(xnew)
    return newinput

############################################################################################################
# Part 3: Wave Reconstruction and Fourier Series
############################################################################################################

def addwaves(waves,const=0,tot_time=1,numpoints=10000):
    times = np.linspace(0,tot_time,numpoints)
    dt = times[1]-times[0]
    signal = np.zeros(numpoints)+const
    for wave in waves:
        amp = wave[0]
        freq = wave[1]
        phase = wave[2]
        w = amp*np.cos(2*np.pi*freq*times-phase)
        signal += w
    
    return signal,times

def plot_fft_1d_signal(signal,flimit=20):
    dft1 = np.fft.fft(signal)
    width = signal.size
    freq1 = np.fft.fftfreq(width,1/width)
    plt.bar(freq1,abs(dft1)/(width))
    plt.xlabel('Frequency')
    plt.ylabel('Amplitude')
    plt.xlim([-flimit,flimit])

def plot_1Dwave_and_fft(waves,const=5,tot_time=1,numpoints=1000,flimit=100,plotwidth=20,plotheight=5):
    signal,timesignal = addwaves(waves,const,tot_time,numpoints)
    f = plt.figure(figsize=(plotwidth,plotheight))
    ax1 = f.add_subplot(121)
    ax1.plot(timesignal,signal)
    plt.xlabel('X axis')
    plt.ylabel('Amplitude')
    
    dft = np.fft.fft(signal)
    dt = tot_time/numpoints
    freqs = np.fft.fftfreq(numpoints,dt)
    plt.subplots_adjust(wspace=0.5)
    ax2 = f.add_subplot(122)
    ax2.bar(freqs,abs(dft)/numpoints)
    plt.xlabel('Frequency')
    plt.ylabel('Amplitude')
    plt.xlim([-flimit,flimit])
    
def play_with_three_sine_waves():
    grid = widgets.GridspecLayout(4,2)
    amp1slider = IntSlider(description="Amp 1",value=1,min=-5,max=5)
    freq1slider = IntSlider(description="Freq 1",value=10,min=1,max=99)
    amp2slider = IntSlider(description="Amp 2",value=1,min=-5,max=5)
    freq2slider = IntSlider(description="Freq 2",value=10,min=1,max=99)
    amp3slider = IntSlider(description="Amp 3",value=1,min=-5,max=5)
    freq3slider = IntSlider(description="Freq 3",value=10,min=1,max=99)
    const_slider = IntSlider(description="AverageVal",value=5,min=-5,max=10)
    grid[0,0] = amp1slider
    grid[0,1] = freq1slider
    grid[1,0] = amp2slider
    grid[1,1] = freq2slider
    grid[2,0] = amp3slider
    grid[2,1] = freq3slider
    grid[3,0] = const_slider
      
    return interact_manual(plot_three_waves,a1=amp1slider,f1=freq1slider,a2=amp2slider,f2=freq2slider,a3=amp3slider,f3=freq3slider,const=const_slider,tot_time=fixed(1),numpoints=fixed(1000))

def DFT(x,shifted=False):
    """
    Compute the discrete Fourier Transform of the 1D array x
    :param x: (array)
    """
    N = x.size
    n = np.arange(N)
    k = n.reshape((N, 1))
    e = np.exp(-2j * np.pi * k * n / N)
    dft = np.dot(e, x)
    if shifted:
        shifted = np.copy(dft)
        shifted[0:int(N/2)] = dft[int(N/2):]
        shifted[int(N/2):] = dft[0:int(N/2)]
        return shifted
    else:
        return dft
    
def plot_three_waves(a1,f1,a2,f2,a3,f3,const=5,tot_time=1,numpoints=1000):
    waves = [(a1,f1,0),(a2,f2,0),(a3,f3,0)]
    plot_1Dwave_and_fft(waves,const,tot_time,numpoints)

############################################################################################################
# Part 4: What is an image
############################################################################################################    

def get_pixel_values(Image):
    return interact_manual(display_df,Image=fixed(Image),
            i=IntSlider(description="Shift Y",value=50,max=240,min=5),
            j=IntSlider(description="Shift X",value=50,max=240,min=5),box=fixed(8),continuous_update=True)

def display_df(Image,i,j,box):
    df = pd.DataFrame(Image)
    df1 = df.iloc[int(i-box/2):int(i+box/2),int(j-box/2):int(j+box/2)]
   # df1_styled = df1.style.background_gradient(cmap='viridis')
    rect = patches.Rectangle((j-box/2,i-box/2),box,box,fill=False,color="yellow")
    f = plt.figure(figsize=(8,8))
    ax2 = f.add_subplot(122)
    ax2.imshow(Image,cmap='gray')
    ax2.add_patch(rect)
    # display df1 as a table
    display(df1)
    
    
############################################################################################################
# Part 5: Fourier Transform of an image
############################################################################################################
def plot_2D_wave():
    return interact(plot2Dsine,fx=IntSlider(description="Freq in x",value=2,min=1,max=50),fy=IntSlider(description="Freq in y",value=2,min=0,max=50),phase=FloatSlider(description="Phase",value=0,min=-3,max=3,step=0.5),amplitude=IntSlider(description="Amplitude",value=2,min=-10,max=10),width=fixed(50),height=fixed(50),continuous_update=False)
    
def fft(f):
    return np.fft.fftshift(np.fft.fft2(f))

def ifft(f):
    return np.fft.ifftshift(np.fft.ifft2(f))

def reconstruct_image(InputImage,N,return_label=False):
    M = N
    (a,b) = InputImage.shape
    f_recon = np.zeros((a,b))
    C = np.zeros((a,b))
    nvc = np.matrix(np.linspace(1,N,N))
    mvc = np.matrix(np.linspace(1,M,M))
    xvc = np.matrix(np.linspace(1,a,a))
    yvc = np.matrix(np.linspace(1,b,a))
    print(nvc.shape)
    print(mvc.shape)
    print(xvc.shape)
    print(yvc.shape)
        #% C [NxM] = sin() [Nx1 X 1xA] * F [AxB] * sin() [Bx1 X 1xM]
    temp1 = nvc.transpose() * xvc
    temp2 = yvc.transpose() * mvc
    C = np.sin ( (np.pi / a ) * temp1 ) * InputImage * np.sin ( (np.pi/b) * temp2 )
    
     #       % F [AxB] = sin()[Ax1 X 1xN] * C[NxM] * sin()[Mx1 X 1xB]
    
    f_recon =  ( np.sin ( (np.pi / a ) * xvc.transpose() * nvc) ) * C * np.sin ( (np.pi/b) * mvc.transpose() * yvc ) 
            
    f_recon = 255 * (f_recon-f_recon.min()) / (f_recon.max() - f_recon.min())
    
    return f_recon
       
def get_fourierwave(fft,i,j,return_label=False):
   # From i,j get correct io,jo
   (width,height) = fft.shape
   midx = int(width/2)
   midy = int(height/2)
   io,jo = midy-i,midx+j
   z = fft[io,jo]
   amp = abs(z)
   phase = cmath.phase(z)
   #print(amp)
   #print(phase)
   xfreqs = np.fft.fftfreq(width,1/width)
   yfreqs = np.fft.fftfreq(height,1/height)
   maxamp = abs(fft[128-1,128+1])
   minamp = -maxamp
   #print(maxamp)
   #print(minamp)
   xv,yv = np.meshgrid(np.arange(width),np.arange(height))
   fourierwave = amp * np.sin(2*np.pi*j*xv/width+2*np.pi*i*yv/height - phase)
   #print(fourierwave.max())
   #print(fourierwave.min())
   (width,height) = fft.shape
   xfreqs = np.fft.fftfreq(width,1/width)
   yfreqs = np.fft.fftfreq(height,1/height)
   xmin,xmax,ymin,ymax = xfreqs.min(),xfreqs.max(),yfreqs.min(),yfreqs.max()
   
   f = plt.figure(figsize=(8,8))
   ax1=f.add_subplot(121)
   ax1.imshow(fourierwave,vmin=minamp,vmax=maxamp)
   ax2=f.add_subplot(122)
   ft_magnitude = abs(fft)/(abs(fft)).size
   ax2.imshow(np.log(ft_magnitude),extent=[xmin,xmax,ymin,ymax])
   ax2.plot([j], [i],'r*')
   #return fourierwave

def colorize(z):
    r = np.abs(z)
    arg = np.angle(z) 
    h = (arg + np.pi)  / (2 * np.pi) + 0.5
    s = 1.0 - 1.0/(1.0 + r**0.3)
    v = 0.8

    c = np.vectorize(hsv_to_rgb) (h,s,v) # --> tuple
    c = np.array(c)  # -->  array of (3,n,m) shape, but need (n,m,3)
    c = c.swapaxes(0,2) 
    c = c.swapaxes(0,1)
    
    return c
from mpl_toolkits.axes_grid1 import make_axes_locatable

def plot2Dsine(fx,fy,phase,amplitude,width=50,height=50):
    x = np.arange(0,width)
    y = np.arange(0,height)
    xv,yv = np.meshgrid(x,y)
    signal = np.zeros((width,height))
    signal = amplitude*np.sin(2*np.pi*fx*xv/width+2*np.pi*fy*yv/height+phase)
    #for i in range(height):
    #    for j in range(width):
    #        signal[i,j] = 1*math.sin(2*math.pi*fx*j/width+2*math.pi*fy*i/height)
            #signal[i,j] = 1*math.sin(2*math.pi*fx*j/width)+1*math.sin(2*math.pi*fy*i/height)

  
    dft2 = fft(signal)/signal.size
    
    xfreqs = np.fft.fftfreq(width,1/width)
    xmin,xmax = xfreqs.min(),xfreqs.max()
    yfreqs = np.fft.fftfreq(height,1/height)
    ymin,ymax = yfreqs.min(),yfreqs.max()
    
    f = plt.figure(figsize=(15,15))
    ax1 = f.add_subplot(121)
    ax1.imshow(signal,vmin=-10,vmax=10)
    ax2 = f.add_subplot(122)
    plt.subplots_adjust(wspace=0.5)
    f1 = ax2.imshow(colorize(dft2),vmin=-np.pi,vmax=np.pi,interpolation='none',extent=[xmin,xmax,ymin,ymax])
    plt.xlabel('Freq in x (/pix)')
    plt.ylabel('Freq in y (/pix)')
    divider = make_axes_locatable(ax2)
    cax = divider.append_axes("right", size="5%", pad=0.5)
    f.colorbar(f1,cax=cax)


def see_fourier_waves(ft):
    return interact(get_fourierwave,fft=fixed(ft),i=IntSlider(description="Freq in Y",value=2,min=-120,max=120),j=IntSlider(description="Freq in X",value=2,min=-120,max=120),return_label=fixed(False),continuous_update=False)

def add_selected_waves(ft):
    return interact_manual(reconstruct_using_fft2d,fft=fixed(ft),ic=IntSlider(description="Shift Y",value=0,min=-120,max=120),jc=IntSlider(description="Shift X",value=0,min=-120,max=120),boxwidth=IntSlider(description="Box Width",value=60,min=10,max=120,step=10),boxheight=IntSlider(description="Box Height",value=60,min=10,max=120,step=10))

def add_using_IFFT(ft):
    return interact_manual(reconstruct_using_ifft2d,fft=fixed(ft),ic=IntSlider(description="Shift Y",value=0,min=-120,max=120),jc=IntSlider(description="Shift X",value=0,min=-120,max=120),boxwidth=IntSlider(description="Box Width",value=60,min=10,max=120,step=10),boxheight=IntSlider(description="Box Height",value=60,min=10,max=120,step=10))

def get_fourier_coefficients(fft,ic,jc,boxwidth,boxheight):
    (width,height) = fft.shape
    istart,jstart = int(width/2-ic-boxheight/2),int(width/2+jc-boxwidth/2)
    iend,jend=istart+boxheight,jstart+boxwidth
    fft_crop = fft[istart:iend,jstart:jend]
    amps = abs(fft_crop)/fft_crop.size
    phases = np.angle(fft_crop)
    xfreqs = np.arange(int(jc-boxwidth/2),int(jc+boxwidth/2))
    yfreqs = np.arange(int(ic-boxheight/2),int(ic+boxheight/2))
    xvf,yvf = np.meshgrid(xfreqs,yfreqs)
    waves = np.dstack((amps,phases,xvf,yvf))
    recaray=np.rec.array(waves,dtype=[('amp',float),('phase',float),('fx',float),('fy',float)])
    waverecord = np.ndarray.flatten(recaray)
    return waverecord

def add_fourierwaves(waves,shape):
    signal = np.zeros(shape)
    width = shape[0]
    height = shape[1]
    x = np.arange(width)
    y = np.arange(height)
    xv,yv = np.meshgrid(x,y)
    for wave in waves:
        amp = wave.amp
        phase = wave.phase
        xfreq = wave.fx
        yfreq = wave.fy
        signal += amp*np.cos(2*np.pi*xfreq*xv/width+2*np.pi*yfreq*yv/height+phase)
    #signal = np.fft.fftshift(signal)
    return signal

def reconstruct_using_fft2d(fft,ic,jc,boxwidth,boxheight):
    tic = time()
    waves = get_fourier_coefficients(fft,ic,jc,boxwidth,boxheight)
    reconstructed_image = add_fourierwaves(waves,fft.shape)
    (width,height) = fft.shape
    xfreqs = np.fft.fftfreq(width,1/width)
    yfreqs = np.fft.fftfreq(height,1/height)
    xmin,xmax,ymin,ymax = xfreqs.min(),xfreqs.max(),yfreqs.min(),yfreqs.max()
   
    f = plt.figure(figsize=(8,8))
    ax1=f.add_subplot(121)
    ax1.imshow(reconstructed_image,cmap='gray')
    ax2=f.add_subplot(122)
    ft_magnitude = abs(fft)/(abs(fft)).size
    rect = patches.Rectangle((jc-boxwidth/2,ic-boxheight/2),boxwidth,boxheight,fill=False)
    ax2.add_patch(rect)
    ax2.imshow(np.log(ft_magnitude),extent=[xmin,xmax,ymin,ymax])
    ax2.plot([jc], [ic],'r*')
    print("Time take to reconstruct: "+str(time()-tic)+" s")

def get_amp_and_phase_at_freq(fft,freq):
    mask=create_concentric_mask(freq,1,fft.shape)
    crop_fft = fft * mask
    amps = abs(crop_fft)
    phases = np.angle(crop_fft)
    
    length = np.count_nonzero(amps)
    amp = amps.sum()/length
    phase = phases.sum()/length
    
    return amp,phase,freq
    
def reconstruct_using_ifft2d(fft,ic,jc,boxwidth,boxheight):
    tic = time()
    mask = np.zeros(fft.shape)    
    (width,height) = fft.shape
    istart,iend,jstart,jend = int(height/2-ic-boxheight/2),int(height/2-ic+boxheight/2),int(width/2+jc-boxwidth/2),int(width/2+jc+boxwidth/2)
    mask[istart:iend,jstart:jend] = 1
    newfft = fft*mask
    newifft = np.fft.ifftshift(ifft(newfft))
    reconstruct = abs(newifft)/(abs(newifft)).max()
    xfreqs = np.fft.fftfreq(width,1/width)
    yfreqs = np.fft.fftfreq(height,1/height)
    xmin,xmax,ymin,ymax = xfreqs.min(),xfreqs.max(),yfreqs.min(),yfreqs.max()
    
    f = plt.figure(figsize=(12,12))
    plt.subplots_adjust(wspace=0.5)
    ax1=f.add_subplot(131)
    ax1.imshow(reconstruct,cmap='gray')
    ax2=f.add_subplot(132)
    ft_magnitude = abs(fft)/fft.size
    rect = patches.Rectangle((jc-boxwidth/2,ic-boxheight/2),boxwidth,boxheight,fill=False)
    ax2.add_patch(rect)
    ax2.imshow(np.log(ft_magnitude),extent=[xmin,xmax,ymin,ymax])
    ax2.plot([jc], [ic],'r*')
    ax3=f.add_subplot(133)
    #ax3.imshow(mask,extent=[xmin,xmax,ymin,ymax])
    ft_magnitude = abs(newfft)/newfft.size
    ax3.imshow(np.log(ft_magnitude),extent=[xmin,xmax,ymin,ymax])
    
    print("Time take to reconstruct: "+str(time()-tic)+" s")

def plot_image_and_its_ft(Image,figsize=(8,8),widthspace=1,withPhase=False):
    (width,height) = Image.shape
    ft = fft(Image)/Image.size
    ft_magnitude = abs(ft)
    ft_phase = np.angle(ft)
    xfreqs = np.fft.fftfreq(width,1/width)
    yfreqs = np.fft.fftfreq(height,1/height)
    xmin,xmax,ymin,ymax = xfreqs.min(),xfreqs.max(),yfreqs.min(),yfreqs.max()
    
    f = plt.figure(figsize=figsize)
    ax1=f.add_subplot(131)
    ax1.imshow(Image,cmap='gray')
    ax2=f.add_subplot(132)
    plt.subplots_adjust(wspace=widthspace)
    if withPhase:
        ax2.imshow(np.log(ft_magnitude),extent=[xmin,xmax,ymin,ymax])
        plt.xlabel('Freq in x (/pix)')
        plt.ylabel('Freq in y (/pix)')
        ax3=f.add_subplot(133)
        ax3.imshow(ft_phase,interpolation='none',extent=[xmin,xmax,ymin,ymax])
        plt.xlabel('Freq in x (/pix)')
        plt.ylabel('Freq in y (/pix)')
        
    else:
        ax2.imshow(np.log(ft_magnitude),extent=[xmin,xmax,ymin,ymax])
        plt.xlabel('Freq in x (/pix)')
        plt.ylabel('Freq in y (/pix)')

def shift_image(Image,shiftX,shiftY):
    Image = np.roll(Image,shiftX,axis=1)
    Image = np.roll(Image,shiftY,axis=0)
    plot_image_and_its_ft(Image,(15,15),widthspace=0.5,withPhase=True)

def rotate_image(Image,angle,mask_Radius=60):
    mask = create_circular_mask(mask_Radius,Image.shape)
    rotated = ndimage.rotate(Image*mask,angle,reshape=False)
    plot_image_and_its_ft(rotated,(15,15),widthspace=0.5,withPhase=True)
    
def translation_property(I):
    return interact(shift_image,Image=fixed(I),shiftX=IntSlider(value=0,min=-128,max=128),shiftY=IntSlider(value=0,min=-128,max=128))
   
def rotation_property(I):
    return interact(rotate_image,Image=fixed(I),angle=IntSlider(value=0,min=-180,max=180),mask_Radius=IntSlider(value=60,min=5,max=240,step=5),continuous_update=False)


############################################################################################################
# Part 6: Convolutions
############################################################################################################

from scipy.signal.windows import gaussian

# get a box window of total length 1000 and width 100
def convolution_illustrate(index, type):
    index = 500 + index
    if type == "box":    
        box1 = getbox(width=300,N=1000,index=500,high=1,low=0)
        box2 = getbox(width=150,N=1000,index=index,high=0.6,low=0)
        box2_static = getbox(width=150,N=1000,index=500,high=0.6,low=0)
        convolution = np.convolve(box1,box2_static,mode='same') / 1000
    elif type == "gaussian":
        box1 = gaussian(1000, std=150)
        box2 = gaussian(10000, std=75)
        cropped_length = 1000
        peak_position_after_cropping = index
        crop_index_start = 5000 - peak_position_after_cropping
        crop_index_end = crop_index_start + cropped_length
        box2 = box2[crop_index_start:crop_index_end]
        box2_static = gaussian(1000, std=75)
        convolution = np.convolve(box1,box2_static,mode='same') / 1000
        
    product = box1*box2
    product_sum = np.sum(product) / 1000
    xaxis = np.linspace(-500,500,1000)
    plt.plot(xaxis, box1, label='$f(x)$')
    plt.plot(xaxis, box2, label='$g(x)$')
    plt.plot(xaxis, convolution, label='$f(x) \circledast g(x)$')
    plt.plot(index-500, product_sum, "ro")
    # plot the legend next to the plot (outside the plot)
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
    plt.xlabel('x')
    plt.show()

def play_with_convolution_illustration():
    return interact(convolution_illustrate, index=widgets.IntSlider(description="x_i",min=-400, max=400, step=1, value=150), type=['box', 'gaussian'])

def create_circular_mask(radius,shape):
    mask = np.zeros(shape)
    xc,yc = int(shape[0]/2),int(shape[1]/2)
    halfwidth = shape[0] // 2
    y,x = np.ogrid[-halfwidth: halfwidth, -halfwidth: halfwidth]
    mask = (x**2+y**2 <= radius**2).astype(np.int_).astype(np.int8)
    
    return mask

def create_concentric_mask(out_radius,thick,shape):
    mask_o = create_circular_mask(out_radius, shape)
    in_radius = out_radius - thick
    mask_i = create_circular_mask(in_radius, shape)
    mask = mask_o - mask_i
    
    return mask


def conv_with_mask_using_fft(i,mask):
    ft_i = fft(i)
    ft_mask = fft(mask)
    ft_conv = ft_i * ft_mask
    conv = ifft(ft_conv)
    conv = abs(conv)/conv.size
    return conv

def conv_with_mask_using_conv2d(i,mask):
    conv = convolve2d(i, mask,mode='same')
    return conv

def plot_convoluted_image(Image,radius,thick,convType='FFT'):
    #radius = IntSlider(value=100,min=20,max=128)
    #thick = IntSlider(value=10,min=1,max=20)
    tic = time()
    mask = create_concentric_mask(radius, thick, Image.shape)
    if convType == 'FFT':
        conv = conv_with_mask_using_fft(Image, mask)
    elif convType == 'Convolution':
        conv = conv_with_mask_using_conv2d(Image,mask)
    
    f = plt.figure(figsize=(12,12))
    plt.subplots_adjust(wspace=0.5)
    ax1=f.add_subplot(121)
    ax1.imshow(conv)
    ax2=f.add_subplot(122)
    ax2.imshow(mask)
    print('Time taken to convolve: '+str(time()-tic)+" s")
    
def play_with_convolution(Image):
    radius = IntSlider(value=100,min=1,max=128)
    thick = IntSlider(value=10,min=1,max=128)
    convType = widgets.Combobox(options=["FFT","Convolution"],value="FFT")
    
    return interact_manual(plot_convoluted_image,Image=fixed(Image), radius=radius, thick=thick,convType=[('FFT','FFT'),('Convolution','Convolution')])

def get_kernel_from_type(kerneltype,size):
    
    if kerneltype=='sharpen':
        kernel = filters.sharpen(size)
    elif kerneltype=='box':
        kernel = filters.box_blur(size)
    elif kerneltype=='gaussian':
        kernel = filters.gaussian_blur(size)
    
    return kernel
    
def plot_image_kernel_conv(Image,kerneltype,size,inputkernel=None):
    if kerneltype=='sobel':
        sobelx,sobely = filters.sobel(size,'xy')
        output_dx = convolve2d(Image, sobelx,mode='same')
        output_dy = convolve2d(Image, sobely,mode='same')
        output = np.sqrt(output_dx**2 + output_dy**2)
        df_dx = pd.DataFrame(sobelx)
        df_dy = pd.DataFrame(sobely)
        print('Sobel derivative in X: \n')
        display(df_dx)
        print('Sobel derivative in Y: \n')
        display(df_dy)
        print('Results cw from top-left: input image, sobel x, output, sobel y')
        show_these((Image,output_dx,output_dy,output))
    
    elif kerneltype=='custom':
        kernel = inputkernel
    else:
        kernel = get_kernel_from_type(kerneltype,size)
  
    if kerneltype != 'sobel':
        output = convolve2d(Image, kernel,mode='same')
        kernel_df = pd.DataFrame(kernel)
        print("The kernel used is: \n")
        display(kernel_df)
        print("Input image and after convolution")
        show_these((Image,output))
    
def play_with_image_kernels(Image):
    dict_ktype = [('Sobel','sobel'),('Sharpen','sharpen'),('Box blur','box'),('Gaussian blur','gaussian')]
    interact_manual(plot_image_kernel_conv,Image=fixed(Image),kerneltype=dict_ktype,size=IntSlider(value=3,min=3,max=51,step=2),inputkernel=fixed(None))

def extract_kernel_and_convolve(Image,isheet):
    kernel = to_array(isheet)
    plot_image_kernel_conv(Image,'custom',isheet.rows,kernel)

def display_button(sheet1):
    button = widgets.Button(description="Run kernel")
    out = widgets.Output()
    def on_button_clicked(_):
          # "linking function with output"
          with out:
              # what happens when we press the button
              data = extract_data(sheet1)
              output = np.zeros((size,size))
              for i in range(size):
                  for j in range(size):
                      output[i,j] = data[i][j]['value']
              print("Heya")
              return output
          print(output)
    # linking button and function together using a button's method   
    button.on_click(on_button_clicked)
    widgets.VBox([button,out])

def get_padded_kernel(kernel,shape):
    bigkernel = np.zeros(shape)
    kwidth,kheight = kernel.shape
    istart,jstart = shape[0]//2-kheight//2,shape[1]//2-kwidth//2
    bigkernel[istart:istart+kheight,jstart:jstart+kwidth] = kernel
    return bigkernel

def demonstrate_conv_theorem(Image,kerneltype,size,justshow=True):
    kernel = get_kernel_from_type(kerneltype,size)
    kernel = get_padded_kernel(kernel, Image.shape)
    convoluted = conv_with_mask_using_fft(Image, kernel)
    if justshow:
        show_these((Image,kernel,np.log(abs(fft(Image))/Image.size),np.log(abs(fft(kernel))/Image.size),convoluted))
    else:
        return convoluted

def convolution_theorem(Image):
    dict_ktype = [('Sharpen','sharpen'),('Box blur','box'),('Gaussian blur','gaussian')]
    interact_manual(demonstrate_conv_theorem,Image=fixed(Image),kerneltype=dict_ktype,size=IntSlider(value=3,min=3,max=51,step=2),justshow=fixed(True))
        
def get_timings_convolution_theorem(Image,kerneltype,size):
    tic=time()
    fft_convoluted = demonstrate_conv_theorem(Image,kerneltype,size,justshow=False)
    time_fft_convoluted = time()-tic
    
    toc = time()
    kernel = get_kernel_from_type(kerneltype,size)
    conv2d_convoluted = conv_with_mask_using_conv2d(Image,kernel)
    time_conv2d_convoluted = time()-toc
   
    print("Time take for FFT algorithm: "+str(time_fft_convoluted)+" s \n"+"Time take for Convolution algorithm: "+str(time_conv2d_convoluted)+" s \n")
    show_these((Image,fft_convoluted,Image,conv2d_convoluted))
         
def compare_fft_convolve(Image):
    dict_ktype = [('Sharpen','sharpen'),('Box blur','box'),('Gaussian blur','gaussian')]
    interact_manual(get_timings_convolution_theorem,Image=fixed(Image),kerneltype=dict_ktype,size=IntSlider(value=3,min=3,max=51,step=2))

def finale():
    Image = get_image('thankyou',256)
    ft = fft(Image)
    interact_manual(reconstruct_using_ifft2d,fft=fixed(ft),ic=fixed(0),jc=fixed(0),boxwidth=IntSlider(description="Box Width",value=1,min=1,max=100,step=1),boxheight=IntSlider(description="Box height",value=1,min=1,max=100,step=1))
    #plt.imshow(Image)
    print(":)")

############################################################################################################
# Part 7: Contrast Transfer Function
############################################################################################################
def get_image_contrast(image):
    michelson_contrast = (np.max(image)-np.min(image))/(np.max(image)+np.min(image))
    return michelson_contrast

def change_contrast(image,contrast):
    assert contrast >= -1 and contrast <= 1, "Contrast must be between -1 and 1"
    new_image = image*contrast
    return new_image

def plot_image_contrast(image, contrast):
    
    new_image = change_contrast(image,contrast)
    new_contrast = get_image_contrast(new_image)
    if contrast < 0:
        vmin, vmax = -1, 0
    else:
        vmin, vmax = 0, 1
    plt.imshow(new_image, cmap="gray", vmin=vmin, vmax=vmax);

def change_contrast_slider(image):
    original_contrast = get_image_contrast(image)
    return interact(plot_image_contrast,image=fixed(image),contrast=FloatSlider(value=0,min=-1,max=1,step=0.01), return_label=fixed(False),continuous_update=True);

def apply_uniform_frequency_contrast(image,contrast):
    # generate a contrast mask with the same shape as the image
    contrast_mask = np.ones(image.shape, dtype=np.complex) * contrast
    # apply the contrast mask to the image
    ft = np.fft.fft2(image)
    ft_contrast = ft * contrast_mask
    # reconstruct the image
    img_contrast = abs(np.fft.ifft2(ft_contrast))
    return img_contrast

# import gaussian filter
from scipy.ndimage import gaussian_filter
def apply_2d_ctf(image, ctf_2d):
    # apply the contrast transfer function to the image
    ft = np.fft.fftshift(np.fft.rfft2(image))
    ctf_2d_half = ctf_2d[:, :ft.shape[1]]
    # smoothen the CTF to avoid ringing artifacts
    ctf_2d_half = gaussian_filter(ctf_2d_half, sigma=1)   

    ft_contrast = ft * ctf_2d_half
    ift = np.fft.irfft2(np.fft.ifftshift(ft_contrast))
    img_ctf = np.real(ift)
    # reconstruct the image
    return img_ctf

def generate_2d_ctf_grid(ctf_1d):
    from scipy import interpolate
    n = len(ctf_1d)
    apix = 1
    freq = np.linspace(1/(n*apix), 1/(2*apix), n, endpoint=True)
    assert len(freq) == len(ctf_1d), "Frequency and CTF must have the same size"
    ctf_interpolate = interpolate.interp1d(freq, ctf_1d, fill_value="extrapolate")

    freq_x, freq_y = np.meshgrid(np.linspace(-1/(2*apix), 1/(2*apix), 2*n, endpoint=True), np.linspace(-1/(2*apix), 1/(2*apix), 2*n, endpoint=True))
    # generate a 2d CTF grid
    freq_2d = np.sqrt(freq_x**2 + freq_y**2)
    # mask freq_2d 
    circular_mask = (freq_2d <= 1).astype(np.int)
    freq_2d = freq_2d * circular_mask
    ctf_2d = ctf_interpolate(freq_2d)
    # apply the CTF to the image      
    return ctf_2d

def apply_1d_ctf(image, ctf_1d):
    ctf_2d = generate_2d_ctf_grid(ctf_1d)
    img_ctf = apply_2d_ctf(image, ctf_2d)
    return img_ctf, ctf_2d

def play_with_frequency_contrast(image_input, low_freq_contrast, mid_freq_contrast, high_freq_contrast):
    from scipy import interpolate
    assert low_freq_contrast >= -1 and low_freq_contrast <= 1, "Low frequency contrast must be between -1 and 1"
    assert mid_freq_contrast >= -1 and mid_freq_contrast <= 1, "Mid frequency contrast must be between -1 and 1"
    assert high_freq_contrast >= -1 and high_freq_contrast <= 1, "High frequency contrast must be between -1 and 1"

    if image_input == "tudelft":
        image = np.flip(mrcfile.open("data/tud_flame.mrc").data, axis=0)
        image = resize(image, (1024, 1024), anti_aliasing=True)
    elif image_input == "cameraman":
        image = get_image("cameraman", size=1024)
    elif image_input == "cryo_em_image":
        image = get_image("cryo_em_image", size=1024)
        # clip the image to show voxels between mean-2*std and mean+2*std
        image = np.clip(image, np.mean(image)-2*np.std(image), np.mean(image)+2*np.std(image))


    # generate a 1D CTF by spline interpolation of low, mid and high frequency contrast
    n = image.shape[0]//2
    apix = 1
    freq = np.linspace(1/(n*apix), 1/(2*apix), n, endpoint=True)

    low_freq_threshold = 1/(n*apix)
    mid_freq_threshold = 0.25
    high_freq_threshold = 0.5

    freq_thresholds = np.array([low_freq_threshold, mid_freq_threshold, high_freq_threshold])
    contrast_thresholds = np.array([low_freq_contrast, mid_freq_contrast, high_freq_contrast])

    interp = interpolate.interp1d(freq_thresholds, contrast_thresholds)
    ctf_1d = interp(freq)
    ctf_1d = np.clip(ctf_1d, -1, 1)

    # plot the 1D CTF, 2D CTF and the image with the CTF applied as subplots
    fig, axes = plt.subplots(1, 3, figsize=(15, 5))
    axes[0].plot(freq, ctf_1d)
    axes[0].set_xlabel("Frequency (1/A)")
    axes[0].set_ylabel("Contrast")
    axes[0].set_title("1D CTF")
    axes[0].set_ylim(-1.2, 1.2)
    # apply the CTF to the image
    ctf_2d = generate_2d_ctf_grid(ctf_1d)
    #ctf_2d = np.clip(ctf_2d, -1, 1)
    xfreqs = np.linspace(-1/(2*apix), 1/(2*apix), 2*n, endpoint=True)
    yfreqs = np.linspace(-1/(2*apix), 1/(2*apix), 2*n, endpoint=True)

    axes[1].imshow(ctf_2d, extent=[xfreqs[0], xfreqs[-1], yfreqs[0], yfreqs[-1]], cmap="gray")
    axes[1].set_xlabel("X Frequency (1/A)")
    axes[1].set_ylabel("Y Frequency (1/A)")
    

    axes[1].set_title("2D CTF")
    img_ctf = apply_2d_ctf(image, ctf_2d)
    
    # normalise img_ctf
    #img_ctf = (img_ctf - img_ctf.min()) / (img_ctf.max() - img_ctf.min())
    percentile_5 = np.percentile(img_ctf.flatten(), 5)
    percentile_95 = np.percentile(img_ctf, 95)
    axes[2].imshow(img_ctf, cmap="gray", vmin=percentile_5, vmax=percentile_95)
    #axes[2].hist(img_ctf.flatten(), bins=100)
    fig.show()
    
    

def play_with_frequency_contrast_slider():
    image_input_dict = {"tudelft": "tudelft", "cameraman": "cameraman", "cryo_em_image": "cryo_em_image"}

    return interact(play_with_frequency_contrast,image_input=image_input_dict,
            low_freq_contrast=FloatSlider(description="Low frequency contrast", value=0,min=-1,max=1,step=0.01),
            mid_freq_contrast=FloatSlider(description="Mid frequency contrast", value=0,min=-1,max=1,step=0.01),
            high_freq_contrast=FloatSlider(description="High frequency contrast", value=0,min=-1,max=1,step=0.01),
            return_label=fixed(False),continuous_update=True);

def plot_image_contrast_fft(image, contrast):
    new_image = apply_uniform_frequency_contrast(image,contrast)
    if contrast < 0:
        vmin, vmax = -1, 0
    else:
        vmin, vmax = 0, 1
    plt.imshow(new_image, cmap="gray", vmin=vmin, vmax=vmax);

def change_contrast_slider_fft(image):
    return interact(plot_image_contrast_fft,image=fixed(image),contrast=FloatSlider(value=0,min=-1,max=1,step=0.01), return_label=fixed(False),continuous_update=True);

def plot_ctf_no_bfactor(voltage_in_kv, defocus_in_um, Cs_in_mm, delta_phi=0):
    voltage = voltage_in_kv * 1000
    defocus_in_angstroms = defocus_in_um * 1e4
    Cs_in_angstroms = Cs_in_mm * 1e7 # convert from mm to Angstroms
    k, CTF = get_CTF_curve(voltage, defocus_in_angstroms, Cs_in_angstroms, delta_phi)
    wavelength = relativistic_lambda(voltage)

    # plot the CTF on one subplot and voltage versus wavelength on another subplot
    fig, axes = plt.subplots(1, 2, figsize=(15, 5))
    axes[0].plot(k, CTF)
    axes[0].set_xlabel("k, Spatial Frequency $\AA^{-1}$")
    axes[0].set_ylabel("Contrast")
    title_text = "CTF for \n Voltage: {} kV, $\lambda$: {:.2f} pm, defocus: {} um, Cs: {} mm".format(voltage_in_kv, wavelength*100, defocus_in_um, Cs_in_mm)
    axes[0].set_title(title_text)
    axes[0].set_ylim(-1.2, 1.2)

    voltage_array = np.arange(100, 300, 1)
    wavelength_array = relativistic_lambda(voltage_array*1e3)
    axes[1].plot(voltage_array, wavelength_array*100, 'k')
    axes[1].plot(voltage_in_kv, wavelength*100, 'ro')
    axes[1].set_xlabel("Voltage (kV)")
    axes[1].set_ylabel("Wavelength (pm)")
    axes[1].set_title("Voltage vs. Wavelength")
    fig.show()

def get_CTF_curve(voltage, defocus, Cs, delta_phi, max_freq=0.5, num_points=1000, B_factor=0):
    k = np.linspace(0, max_freq, num_points)
    wavelength = relativistic_lambda(voltage)
    gamma = (-np.pi/2)*Cs*np.power(wavelength,3)*np.power(k,4)+np.pi*wavelength*defocus*np.power(k,2) 
    CTF = -1*np.sin(delta_phi+gamma) 
    if B_factor != 0:
        CTF *= np.exp(-B_factor*k**2)

    return k, CTF

def plot_ctf(voltage_in_kv, defocus_in_um, B_factor, Cs_in_mm, delta_phi, flip=False):
    voltage = voltage_in_kv * 1000
    defocus_in_angstroms = defocus_in_um * 1e4
    Cs_in_angstroms = Cs_in_mm * 1e7 # convert from mm to Angstroms
    k, CTF = get_CTF_curve(voltage, defocus_in_angstroms, Cs_in_angstroms, delta_phi, B_factor=B_factor)
    exponential_curve = np.exp(-B_factor*k**2)
    
    if flip==True:
        CTF=np.abs(CTF)
    else:
        pass
    
    fig = plt.figure()
    ax = fig.add_subplot(111)    
    # This plots the CTF closed by the envelope function
    plt.plot(k,CTF,color="k")
    # Upper envelope fuction
    plt.plot(k, exponential_curve, color="magenta")    # E = np.exp(-1*B_factor*k_squared)
    # Lower envelope function
    plt.plot(k, -exponential_curve, color="magenta")
    
    title_text = "CTF for \n {} kV, $\lambda$={:.2f} pm, defocus={:.2f}$\mu$, Cs={:.2f}".format(voltage, relativistic_lambda(voltage_in_kv)*100, defocus_in_um,  Cs_in_mm)
    plt.title(title_text)
    ax.set_xlabel("Spatial frequency $\AA^{-1}$", fontsize=15)
    ax.set_ylabel("Contrast", fontsize=15)
    ax.tick_params(axis="both", labelsize=15)
    plt.grid()
    return 

def plot_ctf_simple():
    return interact(plot_ctf_no_bfactor,voltage_in_kv=FloatSlider(description="Voltage (kV)", value=300,min=100,max=300,step=10),
            defocus_in_um=FloatSlider(description="Defocus (um)", value=2.5,min=0,max=10,step=0.1),
            Cs_in_mm=FloatSlider(description="Cs (mm)",value=4.1,min=0,max=5,step=0.1),
            delta_phi=FloatSlider(description="Phase shift (rad)",value=0,min=0,max=2*np.pi,step=0.01),
            return_label=fixed(False),continuous_update=False);

def plot_ctf_with_bfactor():
    return interact(plot_ctf,voltage_in_kv=IntSlider(description="Voltage (kV)",value=300,min=100,max=300,step=10),
                    defocus_in_um=FloatSlider(description="Defocus (um)",value=2.5,min=0,max=10,step=0.01),
                    max_spatial_freq=fixed(0.5),
                    B_factor=FloatSlider(description="B-factor",value=50.0,min=0,max=300,step=10),
                    Cs_in_mm=FloatSlider(description="Cs (mm)",value=4.1,min=0,max=5,step=0.1),
                    delta_phi=FloatSlider(description="Phase shift (rad)",value=0,min=0,max=2*np.pi,step=0.01),
                    continuous_update=False)        
    
def CTF(imsize=[100, 100], DF1=0.5, DF2=None, AST=0.0, WGH=0.10, Cs=2.7, kV=300.0, apix=1.0, B=0.0, rfft=True):
# Generates 2D CTF function
    
    if not np.isscalar(imsize) and len(imsize) == 1:
        imsize = imsize[0]
    
    DF1 = DF1*1000*10  # Convert defocus to Angstrom
    Cs *= 1e7          # Convert Cs to Angstroms
    if DF2 == None or np.isscalar(imsize):
        DF2 = DF1

    AST *= -pi / 180.0
    WL = relativistic_lambda(kV)
    w1 = np.sqrt(1 - WGH * WGH)
    w2 = WGH

    import warnings
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=RuntimeWarning)

        if np.isscalar(imsize):
            if rfft:
                rmesh = np.fft.rfftfreq(imsize)
            else:
                rmesh = np.fft.fftfreq(imsize)
            amesh = 0.0

        else:
            xmesh = np.fft.fftfreq(imsize[0])
            if rfft:
                ymesh = np.fft.rfftfreq(imsize[1])
            else:
                ymesh = np.fft.fftfreq(imsize[1])

            xmeshtile = np.tile(xmesh, [len(ymesh), 1]).T
            ymeshtile = np.tile(ymesh, [len(xmesh), 1])
            rmesh = ne.evaluate("sqrt(xmeshtile * xmeshtile + ymeshtile * ymeshtile) / apix")
            amesh = np.nan_to_num(ne.evaluate("arctan2(ymeshtile, xmeshtile)"))

        rmesh2 = ne.evaluate("rmesh * rmesh")

        # From Mindell & Grigorieff, JSB 2003:
        DF = ne.evaluate("0.5 * (DF1 + DF2 + (DF1 - DF2) * cos(2.0 * (amesh - AST)))")
        Xr = np.nan_to_num(ne.evaluate("pi * WL * rmesh2 * (DF - 0.5 * WL * WL * rmesh2 * Cs)"))

    # CTFim = CTFreal + CTFimag*1j
    CTFim = ne.evaluate("-w1 * sin(Xr) - w2 * cos(Xr)")
    if B != 0.0:  # Apply B-factor only if necessary:
        ne.evaluate("CTFim * exp(-B * (rmesh2) / 4)", out=CTFim)
    return CTFim

def relativistic_lambda(voltage):
    return 12.2643247 / np.sqrt(voltage * (1 + voltage * 0.978466e-6))
    

def plot_ctf_img(image_input, voltage_in_kv=120.0, defocus_in_um=0.1, Bfactor=0.0, Cs_in_mm=4.1):
    if image_input == "tudelft":
        img = np.flip(mrcfile.open("data/tud_flame.mrc").data, axis=0)
        img = resize(img, (1024, 1024), anti_aliasing=True)
    elif image_input == "cameraman":
        img = get_image("cameraman", size=1024)
    elif image_input == "cryo_em_image":
        img = get_image("cryo_em_image", size=1024)
        # clip the image to show voxels between mean-2*std and mean+2*std
        img = np.clip(img, np.mean(img)-2*np.std(img), np.mean(img)+2*np.std(img))


    voltage = voltage_in_kv * 1e3
    defocus = defocus_in_um * 1e4
    Cs = Cs_in_mm * 1e7
    k,ctf_1d = get_CTF_curve(voltage, defocus, Cs, delta_phi=0, num_points = img.shape[0]//2, B_factor=Bfactor)
    exponential_curve = np.exp(-Bfactor * k**2)
    ctf_2d = generate_2d_ctf_grid(ctf_1d)
    # apply ctf_2d to img
    img_ctf = apply_2d_ctf(img, ctf_2d)
    
    if image_input == "cryo_em_image":
        # clip the image to show voxels between mean-2*std and mean+2*std
        sigma_level = 4
        img_ctf = np.clip(img_ctf, np.mean(img_ctf)-sigma_level*np.std(img_ctf), np.mean(img_ctf)+sigma_level*np.std(img_ctf))
   # ft_img_ctf = np.log(np.abs(np.fft.fftshift(np.fft.fft2(img_ctf))))

    # Plot 2x2 subplot grid with original image, CTF corrected image in two row and 1D CTf and 2D CTF in the other row
    fig, ax = plt.subplots(2, 2, figsize=(10, 10))
    ax[0, 0].imshow(img, cmap="gray")
    ax[0, 0].set_title("Original image")
    ax[0, 1].imshow(img_ctf, cmap="gray")
    ax[0, 1].set_title("CTF distorted image")
    ax[1, 0].plot(k, ctf_1d, color='black')
    ax[1, 0].set_xlabel("$k$, Spatial frequency [$\AA^{-1}$]")
    ax[1, 0].set_ylabel("Contrast")
    ax[1, 0].set_title("CTF")
    # set axis limits
    ax[1, 0].set_ylim(-1.2, 1.2)
    ax[1, 0].plot(k, exponential_curve, color="magenta")
    ax[1, 0].plot(k, -1*exponential_curve, color="magenta")   
    freq_max = 0.5
    ax[1, 1].imshow(ctf_2d, cmap="gray", extent=[-freq_max, freq_max, -freq_max, freq_max])
    ax[1, 1].set_title("CTF image")
    ax[1, 1].set_xlabel("$k_x$, Spatial frequency in x [$\AA^{-1}$]")
    ax[1, 1].set_ylabel("$k_y$, Spatial frequency in y [$\AA^{-1}$]")

    plt.show()
       

def plot_ctf_img_interactive():
    # add the image input as a dropdown menu
    image_input_dict = {"tudelft": "tudelft", "cameraman": "cameraman", "cryo_em_image": "cryo_em_image"}
    return interact(plot_ctf_img,
                    image_input=image_input_dict,
                    voltage_in_kv=IntSlider(description="Voltage (kV)",value=300,min=100,max=300,step=10),
                    defocus_in_um=FloatSlider(description="Defocus (um)",value=0.1,min=-10,max=10,step=0.1),
                    Bfactor=FloatSlider(description="B-factor",value=50.0,min=0,max=500,step=10),
                    Cs_in_mm=FloatSlider(description="Cs (mm)",value=4.1,min=0,max=5,step=0.1),
                    continuous_update=True) 

## CTF correction functions

def get_ctf_corrupted_image(image_input, voltage_in_kv=120.0, defocus_in_um=0.1, Bfactor=0.0, Cs_in_mm=4.1):
    if image_input == "tudelft":
        img = np.flip(mrcfile.open("data/tud_flame.mrc").data, axis=0)
    elif image_input == "cameraman":
        img = get_image("cameraman", size=1024)
    elif image_input == "cryo_em_image":
        img = get_image("cryo_em_image", size=1024)
    # resize image to 1024x1024
    img = resize(img, (1024, 1024), anti_aliasing=True)

    voltage = voltage_in_kv * 1e3
    defocus = defocus_in_um * 1e4
    Cs = Cs_in_mm * 1e7
    k,ctf_1d = get_CTF_curve(voltage, defocus, Cs, delta_phi=0, num_points = img.shape[0]//2, B_factor=Bfactor)
    exponential_curve = np.exp(-Bfactor * k**2)
    ctf_2d = generate_2d_ctf_grid(ctf_1d)
    # apply ctf_2d to img
    img_ctf = apply_2d_ctf(img, ctf_2d)
    return img_ctf

def calculate_fourier_transform(img):
    FT = np.fft.fftshift(np.fft.fftn(img))
    return FT

def extract_1D_profile(im, angle):
    import skimage
    # get the center of the image
    center = im.shape[0] // 2
    center_coords = (center, center)
    # from center_coords and angle calculate the end point of the line
    radius = center-2
    # convert angle to radians
    angle_rad = np.deg2rad(angle)
    end_coords = (center + radius * np.cos(angle_rad), center + radius * np.sin(angle_rad))
    # convert to integer coordinates
    center_coords = (int(center_coords[0]), int(center_coords[1]))
    end_coords = (int(end_coords[0]), int(end_coords[1]))
    # get the line
    line = skimage.draw.line(*center_coords, *end_coords)
    # get the 1D CTF
    profile_1d = im[line]
    return profile_1d, center_coords, end_coords, line

def frequency_array(amplitudes=None,apix=None,profile_size=None):
    '''
    Returns a numpy array with elements corresponding to the frequencies of a signal

    Parameters
    ----------
    amplitudes : numpy.ndarray (1,N)
        Amplitudes 
    apix : float
        pixel size, or more generally the size in real units for each index (time, or space)

    Returns
    -------
    freq : numpy.ndarray (1,N)
        Frequencies corresponding to the amplitudes, given the pixelsize
        

    '''
    if amplitudes is not None:
        n = len(amplitudes)
    elif profile_size is not None:
        n = profile_size
    else:
        print("Please enter the size of the array or send the array itself!")
        return 0
    
    if apix is None:
        print("Warning: voxelsize parameter not entered. \n Using apix = 1")
        apix = 1
        
    #freq = np.linspace(1/(apix*n*2),1/(apix*2),n,endpoint=True)
    start_freq = 0
    end_freq = 1/(apix*2)
    freq = np.linspace(start_freq,end_freq,n,endpoint=True)
    return freq


def compute_radial_profile(im):
    '''
    Computes the radial profile of a given volume

    Parameters
    ----------
    vol : numpy.ndarray
        Input array
    center : list, optional
        DESCRIPTION. The default is [0,0,0].
    return_indices : bool, optional
        

    Returns
    -------
    radial_profile : numpy.ndarray (1D)
        Radial profile
        

    '''
    ps = np.abs(np.fft.fftshift((np.fft.fftn(im))))
    im_shape = np.indices(ps.shape)
    y, x = np.indices(ps.shape)
    center = tuple((a - 1) / 2.0 for a in ps.shape[::-1])
    radii = np.sqrt((x - center[0])**2 + (y - center[1])**2)
    radii = radii.astype(int)
    
    radial_profile = np.bincount(radii.ravel(), ps.ravel()) / np.bincount(radii.ravel())
    # exclude corner frequencies
    radial_profile = radial_profile[0:int(ps.shape[0]/2)]
    return radial_profile
    
    
def plot_defocus_and_1D_CTF(input_fourier_transform, defocus, B_factor = 300, delta_phi= 0, window_size_averaging=6, flip=False, xlim_max=0.5):
    # maximize the contrast of the fourier transform so that the CTF is visible
    import scipy
    ft_abs_log = np.log(np.abs(input_fourier_transform))

    # if window_size_averaging > 1 smoothen the image using a box filter
    if window_size_averaging > 1:
        ft_abs_log = scipy.ndimage.filters.uniform_filter(ft_abs_log, size=window_size_averaging)

    num_points = ft_abs_log.shape[0]//2
    k, ctf_1d = get_CTF_curve(voltage=300e3, defocus=defocus*1e4, Cs=0, delta_phi=delta_phi, num_points=num_points, B_factor=B_factor)

    
    # convert the 1d ctf to 2d ctf
    ctf_2d = generate_2d_ctf_grid(ctf_1d)

    if flip:
        ctf_2d = abs(ctf_2d)
        ctf_1d = abs(ctf_1d)
    

    rp_input_ft = compute_2d_radial_profile(ft_abs_log)
    rp_ctf_2d = compute_2d_radial_profile(ctf_2d)

    # normalize the radial profiles
    rp_input_ft = rp_input_ft / np.max(rp_input_ft)
    # plot the 2d fourier transform and 2d ctf side by side and the 1d radial profiles on top of each other in third
    fig, ax = plt.subplots(1, 3, figsize=(10, 5))
    freq_max = 0.5
    ax[0].imshow(ft_abs_log, cmap="gray", extent=[-freq_max, freq_max, -freq_max, freq_max])
    
    ax[0].set_title("Fourier transform")
    ax[0].set_xlabel("$k_x$, Spatial frequency in x [$\AA^{-1}$]")
    ax[0].set_ylabel("$k_y$, Spatial frequency in y [$\AA^{-1}$]")

    # show the 2D CTF
    ax[1].imshow(ctf_2d, cmap="gray", extent=[-freq_max, freq_max, -freq_max, freq_max])
    
    ax
    ax[1].set_title("CTF")
    ax[1].set_xlabel("$k_x$, Spatial frequency in x [$\AA^{-1}$]")
    ax[1].set_ylabel("$k_y$, Spatial frequency in y [$\AA^{-1}$]")

    # plot the radial profiles
    ax2 = ax[2].twinx()
    # reduce mean of input radial profile to 0
    rp_input_ft = rp_input_ft - np.mean(rp_input_ft)
    ax[2].plot(k, rp_input_ft, label="Radial profile input")
    ax[2].set_title("Radial profiles")
    ax[2].set_xlabel("$k$, Spatial frequency [$\AA^{-1}$]")
    ax[2].set_ylabel("Radial profile input")
    ax[2].set_xlim(0, xlim_max)
    ax2.plot(k, ctf_1d, label="Radial profile CTF", color="red")
    ax2.set_ylabel("Radial profile CTF")
    ax2.legend()
    plt.tight_layout()

    fig.show()






    # # flip the ctf_1d if flip is True
    # if flip:
    #     ctf_1d = abs(ctf_1d)
    # fig, ax = plt.subplots(1, 2, figsize=(10, 5))
    
    # ax[0].imshow(ft_abs_log, cmap="gray")
    # ax[0].plot([center_coords[1], end_coords[1]], [center_coords[0], end_coords[0]], color="blue")
    # ax[0].set_title("Fourier transform")
    # ax[1].plot(k, normalized_profile_1d, color="blue")
    # ax[1].set_xlabel("k, Spatial frequency [$\AA^{-1}$]")
    # ax[1].set_ylabel("Intensity")
    # ax[1].set_ylim(ylim_min, ylim_max)
    # # set two Y axes one for intensity profile and one for CTF
    # ax1 = ax[1].twinx()
    # ax1.plot(k, ctf_1d, color="red")
    # ax1.set_title("Angle = {} | Defocus = {} um".format(angle, defocus))
    # ax1.set_ylabel("Contrast")
    
    # # show legend
    # ax[1].legend(["Fourier transform", "CTF"])
    # plt.show()

def plot_defocus_and_1D_CTF_interactive(input_fourier_transform):
    # add the image input as a dropdown menu
    
    return interact(plot_defocus_and_1D_CTF,
                    input_fourier_transform=fixed(input_fourier_transform),
                    defocus=FloatSlider(description="Defocus (um)",value=0.9,min=0.0,max=6,step=0.01),
                    window_size_averaging=IntSlider(description="Smoothing kernel",value=6,min=3,max=20,step=1),
                    B_factor=FloatSlider(description="B factor",value=60,min=0,max=500,step=1),
                    xlim_max = FloatSlider(description="max Frequency",value=0.5,min=0.1,max=0.5,step=0.01),
                    delta_phi = FloatSlider(description="Delta phi",value=0,min=0,max=2*np.pi,step=0.01),
                    continuous_update=True)

def compute_2d_radial_profile(input_ft):


    dim = input_ft.shape
    m = np.mod(input_ft.shape,1)
    
    x,y = np.indices(dim)
    x = np.fft.ifftshift(x)
    y = np.fft.ifftshift(y)
    radii = np.sqrt(x**2 + y**2)
    radii = radii.astype(int)
    
    
    radial_profile = np.bincount(radii.ravel(), input_ft.ravel()) / np.bincount(radii.ravel())
    # exclude corner frequencies
    radial_profile = radial_profile[0:int(input_ft.shape[0]/2)]
    
    return radial_profile
