import numpy as np
from skimage.draw import ellipse, rectangle, circle_perimeter
import matplotlib.pyplot as plt
from skimage.transform import radon,iradon
from scipy.ndimage import rotate, gaussian_filter

from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets

#import mrcfile

def test():
    pass

def make_2D_test_image(size=100):
    im = np.zeros((size,size))
    # left top ring
    rr, cc = ellipse(size//3+size//32, size//3+size//32, size//6,size//6)
    im[rr, cc] = 1
    rr, cc = ellipse(size//3+size//32, size//3+size//32, size//8,size//8)
    im[rr, cc] = 0
    # two lines
    rr, cc = rectangle((size//6*4+size//16, size//6+size//16), extent=(size//32, size//4), shape=im.shape)
    im[rr, cc] = 1
    rr, cc = rectangle((size//6+size//16, size//6*4+size//16), extent=(size//4, size//32), shape=im.shape)
    im[rr, cc] = 1
    # bottom right ring
    rr, cc = ellipse(size//6*4+size//16, size//6*4+size//16, size//32, size//32)
    im[rr, cc] = 1
    return im[::-1]

def power(im):
    F1 =np.fft.fft2(im)
    F2 = np.fft.fftshift( F1 )
    psd2D = np.abs( F2 )**2
    return np.log(psd2D)
    
def imshow(im, im2=None, labels='xz',yaxis=False, ax=None, vmax=1):
    if ax is None:
        fig,ax=plt.subplots(figsize=(7.5,3.5), dpi=100)
    ax.imshow(im, cmap='gray', vmin=0, vmax=vmax, origin='lower', extent=[0, im.shape[1],0,im.shape[0]])
    if im2 is not None:
        ax.imshow(im2, cmap='gray', vmin=0, vmax=vmax, origin='lower', extent=[im.shape[1],2*im.shape[1],0,im.shape[0]])
        ax.set_xlim(0,2*im.shape[0])
        ax.text(im.shape[1]*0.45, im.shape[1]*1.02, 'Real')
        ax.text(im.shape[1]*1.45, im.shape[1]*1.02, 'Fourier')
    if labels=='xz':
        ax.set_xlabel('x'); ax.set_ylabel('z')
    if yaxis:
        ax.scatter(im.shape[0]//2, im.shape[1]//2, color='red')
        ax.text(im.shape[0]//2*1.05, im.shape[1]//2*1.05, 'y', color='red')
    return ax
        
def circular_mask(im):
    w,h = im.shape
    center = (int(w/2), int(h/2))
    radius = min(center[0], center[1], w-center[0], h-center[1]) * 0.9
    Y, X = np.ogrid[:h, :w]
    dist_from_center = np.sqrt((X - center[0])**2 + (Y-center[1])**2)
    mask = dist_from_center <= radius
    return mask    
    
def project(im, angle):
    projection = radon(im, [angle])
    projection_repeated = np.tile(projection,(1, im.shape[0]))
    rotated = rotate(projection_repeated.T,angle, reshape=False)
    m = circular_mask(rotated).astype(float)
    m = gaussian_filter(m,5)
    rotated_masked = rotated * m
    return rotated_masked/im.shape[0]

def rotate_interactive(im, projection=False):
    def func(angle):
        rotated = rotate(im,angle, reshape=False)
        ax = imshow( rotated, yaxis=True  )
        if projection:
            ax.plot(0.4*im.shape[0]*rotated.mean(0), color='red'); ax.set_xlim(0,im.shape[1]); ax.set_ylim(0,im.shape[0]);
        return
    return interact(func, angle=widgets.IntSlider(min=-70, max=70, step=1, value=0, continuous_update=False))
    
def reconstruct(im, angles):
    if 90 in angles and -90 in angles:
        angles = angles[1:]
    im_radon = radon(im, angles)
        #imshow(im_radon)
    im_radon_iradon = iradon(im_radon, angles, interpolation='cubic')#filter='cosine'filter='shepp-logan')
    return im_radon_iradon
    
def reconstruct_interactive(im, fourier=False, fine_increments=False):
    def f(max_tilt_angle, tilt_increment):
        angles = np.linspace(-max_tilt_angle,max_tilt_angle,int(round(2*max_tilt_angle/tilt_increment))+1)
        im_radon_iradon = reconstruct(im, angles)
        if fourier:
            imshow(im_radon_iradon, im2=power(im_radon_iradon)/10)
        else:
            imshow(im_radon_iradon)
        np.set_printoptions(precision=1)
        print('Angles from %.1f to %.1f with %.1f increment adding up %.0f tilts.'%(angles[0], angles[-1], tilt_increment, len(angles)))
        return
    if fine_increments:
        interact(f, 
             max_tilt_angle=widgets.IntSlider(min=60, max=70, step=1, value=65, continuous_update=False), 
             tilt_increment=widgets.FloatSlider(min=0.2, max=4.0, step=0.2, value=1.0, continuous_update=False))
    else:
        interact(f, 
             max_tilt_angle=widgets.IntSlider(min=10, max=90, step=1, value=30, continuous_update=False), 
             tilt_increment=widgets.IntSlider(min=1, max=30, step=1, value=30, continuous_update=False)
            );
    
def fourierslice_interactive(im):
    def f(angle):
        p = project(im, angle)
        imshow(p, im2=power(p)/10)
        return
    interact(f, 
         angle=widgets.IntSlider(min=-70, max=70, step=1, value=0, continuous_update=False), 
            );

def extend_2d_to_3d(im):
    return np.tile(im,(im.shape[0],1,1))
    
def save_as_3d(im, filename):
    im_scale = im-im.min()
    im_scale = im/im.max() * 255
    im3d = extend_2d_to_3d(im_scale)    
    with mrcfile.new(filename, overwrite=True) as mrc:
        mrc.set_data(im3d.astype(np.int8))

# Modify the function to show wireframe lines only along the x-direction
# This code was partially written by ChatGPT, model version: GPT-4
def plot_projections_at_different_angles(angles, projections, title='3D Heatmap', xlabel='Position in 1-D Projection',
                                ylabel='Angle', zlabel='Projection Intensity'):
    """
    Create a 3D wireframe plot of 1-D projections at various angles with lines only along the x-direction.
    
    Parameters:
    - angles (list): List of angles corresponding to each 1-D projection.
    - projections (list of np.array): List of 1-D projections.
    - title (str): Title of the plot.
    - xlabel (str): Label for the x-axis.
    - ylabel (str): Label for the y-axis.
    - zlabel (str): Label for the z-axis.
    
    Returns:
    - ax (matplotlib.axes._subplots.Axes3DSubplot): The 3D subplot object.
    """
    # Make sure all arrays are of the same length by padding with zeros if needed
    max_len = max(len(proj) for proj in projections)
    padded_projections = [np.pad(proj, (0, max_len - len(proj)), 'constant', constant_values=0) for proj in projections]

    # Convert list of arrays to 2D NumPy array
    data = np.array(padded_projections)

    # Create a figure and a 3D axis
    fig = plt.figure(figsize=(10, 6))
    ax = fig.add_subplot(111, projection='3d')
    ax.view_init(elev=30, azim=270)
    ax.set_box_aspect([1, 1, 0.2])

    # Create X, Y meshgrid based on positions and angles
    x = np.arange(0, max_len)
    y = np.array(angles)
    X, Y = np.meshgrid(x, y)

    # Plot the 3D lines along the x-direction
    for i in range(len(y)):
        ax.plot(x, [y[i]] * len(x), data[i, :], color='red')

    # Label the axes and add title
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_zlabel(zlabel)
    #ax.set_title(title)

    # have only three z ticks 
    ax.set_zticks([0,20,40])

    # Turn off the grid for X and Y planes
    ax.xaxis._axinfo["grid"]['linewidth'] = 0.0
    ax.yaxis._axinfo["grid"]['linewidth'] = 0.0

    plt.tight_layout()
    return ax