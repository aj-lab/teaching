import matplotlib.pyplot as plt
import numpy as np
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
from scipy import ndimage

def imshow(im, im2=None, labels='xz',yaxis=False, ax=None, vmax=1):
    if ax is None:
        fig,ax=plt.subplots(figsize=(7.5,3.5), dpi=100)
    ax.imshow(im, cmap='gray', vmin=0, vmax=vmax, origin='lower', extent=[0, im.shape[1],0,im.shape[0]])
    if im2 is not None:
        ax.imshow(im2, cmap='gray', vmin=0, vmax=vmax, origin='lower', extent=[im.shape[1],2*im.shape[1],0,im.shape[0]])
        ax.set_xlim(0,2*im.shape[0])
        ax.text(im.shape[1]*0.45, im.shape[1]*1.02, 'Real')
        ax.text(im.shape[1]*1.45, im.shape[1]*1.02, 'Fourier')
    if labels=='xz':
        ax.set_xlabel('x'); ax.set_ylabel('z')
    if yaxis:
        ax.scatter(im.shape[0]//2, im.shape[1]//2, color='red')
        ax.text(im.shape[0]//2*1.05, im.shape[1]//2*1.05, 'y', color='red')
    return ax


def make_letter(text, size=144):
# Make a random plot...
    fig,ax = plt.subplots(figsize=(2,2))
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    plt.axis('off')

    fig.text(0.23,0.26,text, fontsize=100)
    # If we haven't already shown or saved the plot, then we need to
    # draw the figure first...
    fig.canvas.draw()

    # Now we can save it to a numpy array.
    data = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep='')
    plt.close()
    data = data.reshape(fig.canvas.get_width_height()[::-1] + (3,)).mean(2)
    data -= data.min()
    data /= data.max()
    data = data.astype(bool)[::-1]
    data_inv = (~data).astype(float)
    data_zoom = ndimage.zoom(data_inv, size/data_inv.shape[0])
    return data_zoom 

def simulate_image(image, angle, sigma=1):
    image_rotated = ndimage.interpolation.rotate(image, angle, reshape=False)
    image_rotated_noisy = image_rotated + sigma * np.random.randn(*image.shape)
    return image_rotated_noisy

def simulate_images(image, sigma, N):
    N = N; sigma = sigma
    images_stack = []
    angles = np.random.uniform(0,360,N)
    for angle in angles:
        im_sim = simulate_image(image, angle, sigma)
        images_stack.append(im_sim)
    images_stack = np.array(images_stack)
    return angles,images_stack

def rotate_reference(reference, s):
    angles = np.arange(0,360,s)
    reference_stack = []
    for angle in angles:
        reference_stack.append(ndimage.interpolation.rotate(reference,angle,reshape=False))
    return angles, np.array(reference_stack)

def correlation(image, reference_stack):
    return (image*reference_stack).mean((1,2))

def simulate_interactive(im, projection=False):
    def func(angle,sigma):
        im_sim = simulate_image(im,angle,sigma)
        ax = plt.imshow( im_sim ,cmap='gray', origin='lower')
        return
    return interact(func, angle=widgets.IntSlider(min=-180, max=180, step=1, value=30, continuous_update=False),
                          sigma=widgets.FloatSlider(min=0, max=15, value=1, continuous_update=False) )

class Reconstruct_2D():
    def __init__(self, letter='Q',blurring=2,sampling = 5, n=100, sigma=10, size=64, iterations=3):
        self.size=size
        self.a = make_letter(letter, size=self.size)
        self.n = n
        self.sigma = sigma
        self.angles,self.images = simulate_images(self.a, self.sigma, self.n)
        self.angles_max_cc = None
        self.reconstruction_ini = ndimage.filters.gaussian_filter(self.a, blurring)
        self.reconstruction = self.reconstruction_ini
        self.sampling = sampling
        self.iterations = iterations
        
    def reconstruct(self):
        self.correlations = np.zeros(self.iterations)
        for i in range(self.iterations):
            angles_reference, reference_stack = rotate_reference(self.reconstruction, self.sampling)
            self.angles_max_cc = []
            for image in self.images:
                correlation_all_angles = correlation(image, reference_stack)
                self.angles_max_cc.append(angles_reference[correlation_all_angles.argmax()])
            self.reconstruction = np.zeros_like(self.images[0])
            for angle_max_cc, image  in zip(self.angles_max_cc, self.images):
                image_rotated = ndimage.interpolation.rotate(image, -angle_max_cc, reshape=False)
                self.reconstruction += image_rotated
            self.reconstruction /= len(self.images)
            self.correlations[i] = (self.reconstruction*self.a).mean()
                

def reconstruction_interactive():
    def func(iterations,blurring,sigma):
        a = Reconstruct_2D(letter='Q',blurring=blurring,sampling = 3, n=100, sigma=sigma, size=64, iterations=iterations)
        a.reconstruct()
        fig,ax = plt.subplots(2,2, dpi=120)
        ax[0,0].imshow(a.reconstruction_ini, cmap='gray', origin='lower');ax[0,0].set_title('Initial Model')
        ax[0,1].imshow(a.reconstruction, cmap='gray', origin='lower');ax[0,1].set_title('Reconstruction')
        ax[1,0].scatter(a.angles, a.angles_max_cc, s=1, c='k');ax[1,0].set_xlabel('True angles');ax[1,0].set_ylabel('Max CC angles')
        ax[1,1].plot(a.correlations, c='k');ax[1,1].set_xlabel('Iteration');ax[1,1].set_ylabel('CC')
        plt.tight_layout()
        return
    return interact(func, iterations=widgets.IntSlider(min=1, max=5, value=3, continuous_update=False),
                          blurring=widgets.FloatSlider(min=0, max=10, value=5, continuous_update=False),
                          sigma=widgets.FloatSlider(min=0, max=15, value=2, continuous_update=False) )


def reconstruction_interactive_complex():
    def func(iterations,blurring,sigma,reference,data):
        a = Reconstruct_2D(letter=reference,blurring=blurring,sampling = 3, n=100, sigma=sigma, size=64, iterations=iterations)
        data_img = make_letter(data, size=a.size)
        a.angles,a.images = simulate_images(data_img, a.sigma, a.n)
        a.reconstruct()
        fig,ax = plt.subplots(2,2, dpi=120)
        ax[0,0].imshow(a.reconstruction_ini, cmap='gray', origin='lower');ax[0,0].set_title('Initial Model')
        ax[0,1].imshow(a.reconstruction, cmap='gray', origin='lower');ax[0,1].set_title('Reconstruction')
        ax[1,0].scatter(a.angles, a.angles_max_cc, s=1, c='k');ax[1,0].set_xlabel('True angles');ax[1,0].set_ylabel('Max CC angles')
        ax[1,1].plot(a.correlations, c='k');ax[1,1].set_xlabel('Iteration');ax[1,1].set_ylabel('CC')
        return
    return interact(func, iterations=widgets.IntSlider(min=1, max=5, value=3, continuous_update=False),
                          blurring=widgets.FloatSlider(min=0, max=10, value=5, continuous_update=False),
                          sigma=widgets.FloatSlider(min=0, max=15, value=2, continuous_update=False),
                          reference=widgets.Dropdown( options=['Q', 'O', 'A','random'], value='Q', description='Reference:', disabled=False),
                          data=widgets.Dropdown( options=['Q', 'O', 'A','random'], value='Q', description='Data:', disabled=False))
