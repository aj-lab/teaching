{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font color='DarkBlue'>High-Resolution Imaging (NB4020) \n",
    "----\n",
    "## <font color='CornflowerBlue'>Practical 3: Electron tomography\n",
    "###### Stefan Huber, Alok Bharadwaj, Maarten Joosten, Arjen Jakobi\n",
    "----"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# imports\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "%load_ext autoreload\n",
    "%autoreload 2\n",
    "%matplotlib inline\n",
    "import numpy as np\n",
    "from shared.tomography import *\n",
    "from skimage import io\n",
    "\n",
    "from ipywidgets import interact, interactive, fixed, interact_manual\n",
    "import ipywidgets as widgets\n",
    "\n",
    "import ipyvolume as ipv"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the last practical on Single-Particle Analysis  (SPA) of macromolecules you understood how 2D projection images can be combined to obtain a 3D reconstruction. SPA uses the multiple observations of the same molecule in random orientations to increase the signal-to-noise ratio of the reconstruction. Such an approach requires a purified sample of the molecule of interest, which may mean we lose the native context in which these molecules are found. To study the molecules in their native environment, we may have to look at sections of the cell, or even an entire cellular organelle. In such samples, we cannot control the number of times the molecule is observed in our microscope. SPA becomes difficult to apply for such specimens.\n",
    "\n",
    "Electron tomography follows a different approach than SPA: it is possible to get 3D information from a single unique object by *physically tilting* the sample and acquiring projection images from different tilt angles. Back-projecting those 2D projection images into the 3D domain using the known tilt angles will lead to a 3D reconstruction of the unique object. We shall see how the method works in this practical.\n",
    "\n",
    "We will first study the basic principles of electron tomography by reconstructing a known \"ground truth\" object. For computational efficiency, we will use a 2D image as our ground truth, and obtain 1D projections of it under various tilt angles. We will then use the 1D projections to reconstruct the 2D image.\n",
    "\n",
    "Note on the axis convention: the electron beam hits the sample downwards along the z-axis (from the top in the next image). The tilt-axis is defined as the y-axis pointing through the imaging plane. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "im = make_2D_test_image(150)\n",
    "imshow(im, yaxis=True);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Make sure you get an intuition for the axes, for the direction of the electron beam and how we rotate around the tilt axis.\n",
    "\n",
    "This image is 2D, but you can imagine that it is repeated into the y-direction infinitely. The circle would become a tube (like a microtubule), the line would become a sheet, and the dot becomes a rod.\n",
    "\n",
    "<img src=\"images/3dim.png\" alt=\"3D Image\" style=\"width: 400px;\"/>\n",
    "\n",
    "This image shows a surface rendering of the above image repeated along the y-axis, which leads to a 3D image. It was generated with the free software ChimeraX. We sometimes call these 3D images 'densities', as they are related to the density of the electro-static potential around the molecules. A convention is that electron-dense objects have positive contrast (are 'white') during processing. \n",
    "\n",
    "A typical way how we look at 3D densities is to draw surfaces at a threshold value. Below is a interactive 3D rendering of this density."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "V = extend_2d_to_3d(im)\n",
    "\n",
    "ipv.figure()\n",
    "ipv.plot_isosurface(V, level=0.95)\n",
    "ipv.view(-90); ipv.xyzlim(0,im.shape[0]); ipv.xyzlabel('y', 'z', 'x')\n",
    "#ipv.pylab.style.box_on()\n",
    "ipv.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can rotate the goniometer (hardware that can rotate an object) in an electron microscope around the y axis up to +-60 or 70 degrees (widget below)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rotate_interactive(im);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Why are we limited in tilt to 60 or 70 degrees? One could design a goniometer that tilts 90 degrees as well. The typical sample is deposited on an electron microscopy grid with gridbars surrounding the observation areas for stability.  \n",
    "\n",
    "The image below shows a typical 3 mm grid used for sample preparation [2]. At tilts higher than 60-70 degrees, the grid bar will obscur the field of view. Additionally images at high tilt have increased apparent thickness, which decreases quality due to additional inelastic scattering.\n",
    "\n",
    "<img src=\"images/gridbars.PNG\" alt=\"3D Image\" style=\"width: 400px;\"/>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In a typical cryo-ET experiment, we take a projection image for each tilt angle from -60 to 60 degrees with typical tilt increments of 1-3 degrees between tilt angles. These images are projections of the object along the z axis. In our simplified example, the projections will not be 2D images, but 1D profiles (red curve, widget below)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rotate_interactive(im, projection=True);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Generate \"tilt series\" dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that you are familiar with the basic principles of tomography and the conventions used in this tutorial, we can generate a small test dataset for you to work with. We can project our 2D image in every angle from -60 to 60 degrees with an increment of 3 degrees to generate a tilt series similar to a dataset you would get from a cryoET experiment.\n",
    "\n",
    "The python variable 'projections' contains all 1D projections of our 2D image for the given 'angles' (red curves, plotted below). Your task is to attempt reconstruction of the original signal from only those 1D curves of data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "angles = np.arange(-60,60+1,3)\n",
    "projections = radon(im, angles).T\n",
    "\n",
    "plot_projections_at_different_angles(angles=angles, projections=projections, \n",
    "                                     xlabel='x [pixels]', ylabel='Angle [deg]', zlabel='Intensity');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Task 1: Image reconstruction by backprojection"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The goal of cryoET is to reconstruct a 3D density from a tilt series of 2D projections.\n",
    "In our simplified example, the goal is to reconstruct the original 2D image from a tilt series of 1D projections.\n",
    "\n",
    "The most intuitive way to do this is a backprojection approach. Each 1D projection is repeated along a new axis to generate a 2D \"smear\", which is then rotated by the corresponding tilt angle. All those 2D \"smears\" are added up to reconstruct the underlying 2D signal.\n",
    "\n",
    "TODO:\n",
    "\n",
    "    1. Take a single 1D projection from the 'projections' variable and repeat (backproject) it along a new axis. (some help for that is in the next field under 'useful functions')\n",
    "    2. Rotate the result by the corresponding tilt angle. Visualise the rotated backprojection and verify that it worked.\n",
    "    3. Write a loop to do this for all the 1D projections in the 'projections' variable.\n",
    "    4. Reconstruct the original 2D image from this by adding up all backprojections. Plot it side-by-side with the original 'im' image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Useful functions\n",
    "from scipy.ndimage import rotate\n",
    "\n",
    "def repeat_line(line, N):\n",
    "    \"\"\"this function repeats the given 1D array (line) along a new axis N times.\n",
    "    The resulting array is a 2D array of shape (N, line.shape[0]).\n",
    "\n",
    "    Remember you can print the shape of any array using array.shape\n",
    "    \"\"\"\n",
    "    repeated_line = np.repeat(line[np.newaxis,:], N, axis=0)\n",
    "    # print(repeated_line.shape)\n",
    "    return repeated_line\n",
    "\n",
    "\n",
    "def imshow(image, xlabel=\"\", ylabel=\"\", title=\"\"):\n",
    "    \"\"\"show an image, in grayscale with the origin at the bottom\"\"\"\n",
    "\n",
    "    _, ax = plt.subplots()\n",
    "    ax.imshow(image, cmap=\"gray\", origin=\"lower\")\n",
    "    ax.set_xlabel(xlabel)\n",
    "    ax.set_ylabel(ylabel)\n",
    "    ax.set_title(title)\n",
    "    return 0\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 1.-4. Your Code\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "## <font color='Teal'>Question 1:\n",
    "\n",
    "How does the result compare to the original image? What can we do to improve the reconstruction?\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Your answer here\n",
    "(double click this cell to edit)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Task 2: Make a filtered reconstruction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One method used to reduce the artifacts introduced in the backprojection algorithm is to filter the projections first. We want to get rid of some of the strong low-resolution fourier components in the image, which cause the blurring. To do this we need to decide on a method of high-pass filtering. Since you already saw filtering in the first practical, we will leave it up to you to design a function that accomplishes the task. The filter you design should be applied in Fourier space and reduce the Fourier components close to 0 frequency, while leaving the high-frequency components alone (e.g. by multiplying low-frequency components with 0 and high-frequency components with 1). You could use a smooth function, a linear function, or a step function with a threshold.\n",
    "\n",
    "Once you have designed a filter, apply it to the projections and reconstruct the image again. Compare the result to the original image and the unfiltered reconstruction.\n",
    "\n",
    "    TODO\n",
    "        1. Design a filter to apply to the projections.\n",
    "        2. repeat the algorithm from task 1 to reconstruct the 2D image from the filtered projections.\n",
    "        3. show the resulting reconstruction side-by-side with the original image and the unfiltered reconstruction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Useful functions for doing the filtering of the backprojection in fourier space\n",
    "def calculate_fourier_transform(line):\n",
    "    return np.fft.rfft(line)\n",
    "\n",
    "def calculate_inverse_fourier_transform(ft_line):\n",
    "    return np.fft.irfft(ft_line)\n",
    "\n",
    "def get_frequencies(line, pixel_size=1):\n",
    "    \"\"\"\n",
    "    This function returns the frequencies of a real-space line\n",
    "    \"\"\"\n",
    "    freq = np.fft.fftfreq(line.shape[0], d=pixel_size)\n",
    "    return freq\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 1.-3. Your Code\n",
    "\n",
    "# Design a filter\n",
    "projection = projections[0]\n",
    "projection_FFT = calculate_fourier_transform(projection)\n",
    "frequencies = get_frequencies(projection)\n",
    "my_filter = \n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Reconstruction in Fourier Space"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Interactive: Effect of tilt increment and maximum tilt angle"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We programmed some interactive widgets for you to explore artefacts expected in reconstructed tomograms. You do not have to fill out answers for this interactive part.\n",
    "\n",
    "The first widget lets you play with tilt-increment and maximum tilt angle to get a feel for the artefacts you can expect when doing tomography. Try it out below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reconstruct_interactive(im);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Look at the consequences of a tilt angle < 90 degrees. \n",
    "\n",
    "What artefacts arise from this? Which types of objects in the image are especially affected? Think about objects in a biological cell. How will they look like with this 'missing wedge' artefact (e.g. microtubules, vesicles, protein complexes)\n",
    "\n",
    "Below you see a reconstruction of our test image with typical values of 1 degree increment and 60 degrees tilt, extended to 3D. This looks very similar to a real reconstruction from biological specimens with typical artefacts."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "im_60 = reconstruct(im, angles = np.arange(-60,60+1,1))\n",
    "\n",
    "V = extend_2d_to_3d(im_60)\n",
    "ipv.figure()\n",
    "ipv.plot_isosurface(V, level=0.95)\n",
    "ipv.view(-90); ipv.xyzlim(0,im_60.shape[0]); ipv.xyzlabel('y', 'z', 'x')\n",
    "ipv.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fourier view on tomography"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the two tasks before you were reconstructing your tomogram in real space. This is not very efficient, because data has to be backprojected from 1D into 2D for each tilt and then added up for reconstruction. \n",
    "\n",
    "\n",
    "A more elegant way to view projections is to use the Fourier Slice Theorem. We programmed an interactive widget for this below. You can see that a 1D line in Fourier Space is equivalent to a projection image in Real Space."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fourierslice_interactive(im);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below you can see a widget that gives you an intuitive impression how reconstructed tomograms look in Fourier Space. They are essentially sums of the angled 1D lines you have seen above. Viewing the tomogram in Fourier Space gives you a better look on the effect of maximum tilt angle and tilt increment on Fourier Space completeness. The 'missing wedge' also gets an intuitive interpretation in Fourier Space."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reconstruct_interactive(im, fourier=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Task 3: How fine do you have to sample?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The final remaining question is how fine we have to sample the tilt angles to get a complete Fourier Space. The widget below lets you play with the tilt increment to get a feel for the effect on Fourier Space completeness. After that we will derive a formula for the minimum tilt increment to get a complete Fourier Space."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reconstruct_interactive(im, fourier=True, fine_increments=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Derivation of the number of tilt samples to reach Fourier completeness\n",
    "Let's assume we image a (2D) sample with thickness $D \\AA$ and we make (1D) projections of this sample in a tilt series. As you know, these projections correspond to slices through the (2D) Fourier space of the sample, according to the Fourier Slice Theorem. The thickness of the slice in Fourier space is given by the inverse of the thickness of the sample in real space:\n",
    "\n",
    "$$\\Delta q = \\frac{1}{D}$$\n",
    "\n",
    "During the lecture you discussed that a given tilt angle increment $\\Delta \\theta$ fills the Fourier Space of the sample up to a resolution $r$ as shown in the image below:\\\n",
    "<img src=\"images/crowther-01.png\" alt=\"3D Image\" style=\"width: 600px;\"/>\n",
    "\n",
    "When you consider the geometry of this situation, you can find how the tilt angle required to fill Fourier space up to the desired resolution $r$ is related to the thickness of the sample $D$ and the resolution $r$. For small angles, this leads to the Crowther criterion, which you will derive in the following question.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "## <font color='Teal'>Question 2:\n",
    "\n",
    "Show that the number of tilt angles $m$ needed to fill the Fourier Space up to a resolution $r$ is given by:\n",
    "$$m \\propto \\frac{D}{r}$$\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Your answer here\n",
    "(double click this cell to edit)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## References\n",
    "\n",
    "[1] Mahamid, Julia and Wolfgang Baumeister. “Cryo-electron tomography : the realization of a vision.” (2012).\n",
    "\n",
    "[2] CHRISTOPHER J. RUSSO, LORI A. PASSMORE, \"Ultrastable gold substrates for electron cryomicroscopy\" SCIENCE12 DEC 2014 : 1377-1380\n",
    "\n",
    "[3] Methods in Cell Biology Volume 79, 2007, Pages 741-767 Methods in Cell Biology Structure Determination In Situ by Averaging of Tomograms "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
