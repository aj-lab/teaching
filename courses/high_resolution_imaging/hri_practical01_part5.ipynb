{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font color='DarkBlue'>High-Resolution Imaging (NB4020) \n",
    "----\n",
    "## <font color='CornflowerBlue'>Practical 1: Introduction to Fourier Optics and Image Processing\n",
    "    \n",
    "## 2D Fourier Analysis\n",
    "\n",
    "In this section, we shall explore the concept of 2D Fourier Analysis in some more detail. First, let us take a step back and look at two-dimensional waves and how they relate to the 2D frequency spectrum (their Fourier space representation)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Frequencies varying along space\n",
    "The equation of the wave in 2D is:\n",
    "\n",
    "<a id='equation_9'></a>\n",
    "$\n",
    "\\begin{equation}\n",
    "    f(x,y) = A \\cdot sin(2 \\pi f_x \\cdot x + 2 \\pi f_y \\cdot y + phase)  \\tag 9\\\n",
    "\\end{equation}\n",
    "$\n",
    "\n",
    "Run the following interactive plot to change the amplitude, phase, and frequencies in x and y direction of this 2D wave. Running a DFT algorithm on a 2D signal can be time-consuming, and hence the frequency spectra are all calculated using the Fast Fourier Transform algorithm, implemented in the [NumPy](https://numpy.org/) package, [```np.fft.fft2```](https://numpy.org/doc/stable/reference/generated/numpy.fft.fft2.html)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from shared.misc import *\n",
    "plot_2D_wave();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Just as we have seen for the 1D case, in 2D the Fourier space is also centrosymmetric and the two points in the frequency spectrum are simply each other's complex conjugates. Hence they move in opposite directions. Let us now look at the Fourier Spectrum of a real image. \n",
    "\n",
    "Run the following code. In the center panel, you find the magnitude of the Fourier Transform (amplitude spectrum, or power spectrum) of the image, while in the right panel you can see the phase component of the Fourier transform (phase spectrum). \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "I = rgb2gray(plt.imread('data/cameraman.bmp'))\n",
    "\n",
    "plot_image_and_its_ft(I,(12,12),widthspace=0.5,withPhase=True);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The bright streaks in the amplitude spectrum show the different angled lines which are dominant in the image. As explained before, each points in the spectrum corresponds to a wave in real space. This can be visualised better in the following Interactive. Move the red star around the spectrum to see the corresponding wave in real space. \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "see_fourier_waves(fft(I));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "###  Reconstruction of 2D signals\n",
    "Since we already know that we can add waves to reconstruct a signal (and this of course is true in 2D just as well as in 1D), let us instead have a closer look at the effect that selecting a particular frequency range has on the final image. This is similar to how we reconstructed different 1D sine waves to get any 1D signal. \n",
    "\n",
    "In this interactive plot, all the waves corresponding to the frequencies (or Fourier coefficients) inside the rectangle in the FT (right panel) are added to obtain a corresponding image in real space (left panel)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "add_selected_waves(fft(I));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, adding waves in real space yields the image as expected. However, adding waves in this way is time-consuming. A much more efficient method to perform the same operation is using Inverse Fast Fourier Transform algorithm implemented in [```np.fft.fft2```](https://numpy.org/doc/stable/reference/generated/numpy.fft.fft2.html). \n",
    "\n",
    "To reconstruct the image using every frequency component within the rectangle in the Fourier transform above, all we need to do is make all other coefficients in the Fourier transform equal to zero. We can then take the inverse Fourier Transform to generate the image on the left. Run the following code and see how quickly the result is obtained as opposed to the previous method. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "add_using_IFFT(fft(I));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Properties of Fourier Transforms\n",
    "\n",
    "These are some important properties of Fourier Transforms that are useful to remember. \n",
    "\n",
    "* __Translation Property:__  \n",
    "A translation of signals in real space does not change the amplitude spectrum of the Fourier transform, but has an effect on the phase spectrum. This means that shifting an image (or having a time delay in 1D signals) does not change the the amplitudes of the Fourier coefficients, but it does alter the associated phases. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "translation_property(I);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* __Rotation Property__\n",
    "\n",
    "When the image is rotated along an axis perpendicular to the image plane, it rotates both the amplitude and phase spectrum of the Fourier transform equally. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rotation_property(I);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "## <font color='Teal'>Question 3:\n",
    "\n",
    "In the interactive plot above, play with the mask radius.\n",
    "\n",
    "* What might be the benefit of using a mask for image processing in the fourier domain?\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Your answer here\n",
    "(double click this cell to edit)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the next section, we shall learn about convolutions, kernels and the convolution theorem. \n",
    "\n",
    "[Prev: 2-D Fourier Analysis](hri_practical01_part4.ipynb)    \n",
    "[Next: Convolutions](hri_practical01_part6.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.7.11 ('MD')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  },
  "vscode": {
   "interpreter": {
    "hash": "4b84bbe0ef3eb9a9935eefc88be2a4e7bf82e7903c59e692e7defe5cb9fd7e95"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
