{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font color='DarkBlue'>High-Resolution Imaging (NB4020) \n",
    "----\n",
    "## <font color='CornflowerBlue'>Practical 1: Introduction to Fourier Optics and Image Processing\n",
    "    \n",
    "## Frequency Spectrum\n",
    "\n",
    "As we learned in the last section, any 1-D signal can be split into an arbitrary number of sinusoidal waves. In this section, we shall see how to analyse these signals in terms of the distribution of these waves across a spectrum of frequencies. \n",
    "\n",
    "Again, let's consider a simple sine wave amplitude 1, frequency 10 and an initial phase of 0. Run the following code to plot the wave."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from shared.misc import *\n",
    "\n",
    "amp,freq,phase = 1,10,0\n",
    "w1 = [(amp,freq,phase)]\n",
    "signal1,time = addwaves(w1,const=5,tot_time=1,numpoints=1000)\n",
    "plt.plot(time,signal1)\n",
    "plt.xlabel('Time (second)')\n",
    "plt.ylabel('Amplitude');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The signal is recorded for 1 second and has 10 cycles through it, thus it has a frequency of 10 Hz. The average value of the signal is around 2 and oscillates between 1 and 3. DC component of our signal is the average value and is equal to 2. To find the \"frequency spectra\" of this we can use a \"Discrete Fourier Transform\", which is a mathematical operation given by\n",
    "\n",
    "<a id='equation_8'></a>\n",
    "$\n",
    "\\begin{equation}\n",
    "    F(k) = \\frac{1}{P} \\cdot \\sum\\limits_{m=0}^{P-1} b(k) \\cdot e^{i \\cdot \\frac{-2 \\pi k \\cdot m}{P}}  \\tag 8\\\n",
    "\\end{equation}\n",
    "$\n",
    "\n",
    "This gives the $complex$ Fourier coefficient of the $k^{th}$ frequency. The magnitude and phase of the wave can be extracted directly from this complex coefficient. \n",
    "\n",
    "First re-run the previous plot by changing the ```const``` variable in \n",
    "\n",
    "```signal1,time = addwaves(w1,const=0,tot_time=1,numpoints=1000)``` to 5, i.e.  ```const=5```. \n",
    "\n",
    "You will see that, instead of -1 to 1, the wave now oscillates between 4 and 6, with an average value of 5. \n",
    "\n",
    "Now run the code below to get the amplitudes at different frequencies. To increase or decrease the frequency range displayed in the x-axis, change the 'flimit' parameter. By default, it is set at 80. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_fft_1d_signal(signal1,flimit=80);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see that there is a peak at 0 Hz, with a value of 5 (which is the new value of ```const``` in the above code). This is the average value we get by integrating over the whole time period. The integral of a sine wave over a full time period is zero, thus the amplitude of the frequency spectra at 0 Hz is it's DC power. By changing the ```const``` term above, you have changed the power of the DC component.\n",
    "\n",
    "We also see that the plot has a symmetry around the 0 Hz line. The reason for this stems from the complex nature of the DFT signal: The two peaks are the complex conjugates of one another - they have the same amplitude, but opposite phase. \n",
    "\n",
    "We see a peak at 10 Hz and -10 Hz with a value of 0.5. Together they have a value of 1. This means there is one full cycle of 10 Hz frequency present in our signal, which can be verified easily from above. \n",
    "\n",
    "Let us make it a bit more complex: Suppose we add a 20 Hz and 40 Hz signal to the original wave, then the following plot shows what we get."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "w2 = [(1,10,0),(1,20,0),(1,40,0)]\n",
    "plot_1Dwave_and_fft(w2,flimit=100);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the signal looks complicated. We cannot know anymore which frequencies are present in the signal just by looking at its time-varying plot.    \n",
    "The frequency spectrum on the other hand, clearly show distinct peaks at 10 Hz, 20 Hz and 40 Hz!  \n",
    "\n",
    "<!-- The maximum frequency in the frequency spectra is called the Nyquist frequency. The formula to calculate the Nyquist frequency is\n",
    "$\n",
    "\\begin{equation} \n",
    "    \\frac{1}{2 n d}\n",
    "\\end{equation}\n",
    "$. \n",
    "where: \n",
    "- $n$ is the number of samples \n",
    "- $d$ is the sample spacing\n",
    " -->\n",
    " \n",
    " Within the time window we measure the signal, there is a limited frequency that can be observed. One way to think about this is that we need to measure a pure sine wave at at least 2 positions to know its frequency, or as Claude Shannon put it in 1949:\n",
    "\n",
    ">\"If a function f(t) contains no frequencies\n",
    "higher than W cps, it is completely determined by giving\n",
    "its ordinates at a series of points spaced 1/2W seconds\n",
    "apart.\"\n",
    "\n",
    "meaning that a signal with maximum frequency W Hz is completely determined if we measure every 1/(2W) seconds. This theorem also became known as Shannon-Nyquist sampling and it is very useful in imaging to determine how fine the pixel size of an image has to be to observe features of a resolution of 2x that pixel size or larger.\n",
    "\n",
    " "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now try changing these amplitudes and frequencies yourself and observe how the frequency spectrum changes!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "play_with_three_sine_waves();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "## <font color='Teal'>Question 2:\n",
    "\n",
    "* Given a recorder that can measure 15 timepoints in 100s. What is the maximum frequency signal component that we can recover?\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Your answer here\n",
    "(double click this cell to edit)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the next section, we will dive into 2D signals and how they relate to images. We shall apply the concepts we have learned here to such signals as well. \n",
    "\n",
    "[Prev: Fourier Series](hri_practical01_part2.ipynb)    \n",
    "[Next: 2-D Fourier Analysis](hri_practical01_part4.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  },
  "vscode": {
   "interpreter": {
    "hash": "4b84bbe0ef3eb9a9935eefc88be2a4e7bf82e7903c59e692e7defe5cb9fd7e95"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
