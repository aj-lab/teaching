{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font color='DarkBlue'>High-Resolution Imaging (NB4020) \n",
    "----\n",
    "## <font color='CornflowerBlue'>Practical 2: Single-Particle Reconstruction\n",
    "###### Maarten Joosten, Stefan Huber, Alok Bharadwaj, Arjen Jakobi\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# imports\n",
    "from shared.singleparticle import *\n",
    "from emmer.ndimage.filter.filter_utils import calculate_fourier_frequencies, tanh_filter\n",
    "import warnings\n",
    "warnings.filterwarnings('ignore')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Description of the problem\n",
    "\n",
    "The goal of Single-Particle Analysis (SPA) methods is to deduce the 3D structure of a structurally homogenous \"particle\", for example a protein, from cryo-EM images. With powerful electron microscopes, we can directly image objects of this size (on the order of ten nanometers diameter). One typical cryo-EM image contains several hundred particles contained in a thin frozen film of buffer. These particles adopt random positions and orientations. Typically a dataset of thousands of such images is recorded. This gives 100,000's to 1,000,000's of unique 2D views of the underlying 3D \"particle\".\n",
    "\n",
    "The problem to solve is to find the 3D density $A$ that gave rise to the set of observed 2D images ${X_0, X_1, ..., X_n}$ This is a difficult problem because:\\\n",
    "a) we don't know the set of positions and orientations $Y$ of the individual particles in the 2D images\\\n",
    "b) the projection operation of the EM microscope means we are not getting direct information about the 3D density\\\n",
    "c) The 3D density is typically on the order of $256^3$ voxels. In principle we would need to tune any of the 16,777,216 voxels to optimize the 3D density to correspond best with the 2D images.\\\n",
    "d) The images are extremely noisy (SNR~0.1-0.01), and corrupted by a Contrast Transfer Function (CTF)\n",
    "\n",
    "To make this problem tractable computationally, we iteratively cycle between estimating the underlying 3D density $A$ and assigning the positions and orientations $Y$ of the 2D images $X$. We start with a very rough initial model of the 3D density. In the computer, we generate 2D projections of it in all directions and compare the raw 2D images $X$ with these projections. From the best match we obtain the most likely positions and orientations $Y_i$ for each raw 2D image. These positions and orientations $Y$ can now be used to update the 3D density $A$ by backprojecting the 2D particle images with those parameters. [2]\n",
    "\n",
    "<img src=\"images/nb4012_pm_scheme.png\" alt=\"3D Image\" style=\"width: 800px;\"/>\n",
    "\n",
    "In this practical we will write an algorithm to perform SPA on an example problem in 2D instead of 3D (to make it computationally easier). We will guide you through the steps required to produce a reconstruction from the data, but you will have a lot of freedom to explore creative solutions as well. First, we will provide you with a training set of images to help you build your algorithm and once you are convinced it works correctly we will ask you to test it on unknown test data. At the end you will submit the finished algorithm and the solution to the test data.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Mathematical background\n",
    "\n",
    "Let's write down in mathematical terms what we described in the previous section. In this example we have an object $A$ (2-dimensional to keep computational cost down) that gives rise to a number of images $X$ like:\n",
    "\n",
    "<a id='equation_1'></a>\n",
    "$\n",
    "\\begin{equation}\n",
    "X_i = R^{\\theta_i} A + \\sigma G_i \\tag 1\n",
    "\\end{equation}\n",
    "$\n",
    "where $X_i$ is the observed image, $A$ is the unknown object, $R^{\\theta_i}$ represents a rotation of the image by an unknown angle $\\theta_i$ (in this case, $\\theta_i$ acts as our unknown orientation $Y$), $G_i$ is independent Gaussian noise added to the image with a standard deviation of $\\sigma$. If we knew $\\theta_i$ we could recover $A$ as:\n",
    "\n",
    "<a id='equation_2'></a>\n",
    "$\n",
    "\\begin{equation}\n",
    "A \\approx (R^{\\theta_i})^{-1}X_i \\tag 2\n",
    "\\end{equation}\n",
    "$\n",
    "\n",
    "However, this would not be a very good reconstruction if the standard deviation of the noise is very high. To make it better we can average over multiple images:\n",
    "\n",
    "<a id='equation_3'></a>\n",
    "$\n",
    "\\begin{equation}\n",
    "A \\approx \\frac{1}{N} \\Sigma_{i=0}^N (R^{\\theta_i})^{-1} X_i \\tag 3\n",
    "\\end{equation}\n",
    "$\n",
    "\n",
    "This partially averages out the noise. But in order to do this, we will need to estimate $\\theta_i$ for each image."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Generating an example object\n",
    "We assume to have an underlying 2D \"density\" $A_{2D}$ instead of a 3D particle density $A_{3D}$. This reduces the unknown three projection angles in 3D to just one projection angle in 2D (also called in-plane rotation). We also do not allow xy-translations and assume that our 2D images $X$ are perfectly centered with respect to $A$. We create a 2D particle density $A$ that shows the letter Q. Note that the particle has positive contrast against the background (it is white). This is a common convention in the field, since people are most comfortable to think of their signal as being positive. Phase contrast in real cryo-EM images is negative though, so particles are black. Real images are therefore usually inverted during processing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "im = make_letter('Q', size=128)\n",
    "imshow(im);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Adding rotations and noise\n",
    "The images are rotated and corrupted with noise from various sources. Shot noise from the electron beam and Poisson noise from the detector. Often it is assumed that the total noise contribution can be described by independent Gaussian noise with a certain standard deviation $\\sigma$. Below you can explore what the noise at higher levels does to the data and why we will need many particles for a good reconstruction. In real data, the value of $\\sigma$ would typically be around 10-100."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "simulate_interactive(im);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Simulating stack of training data\n",
    "Now that we know how the image formation works, we will generate a stack of simulated images that you will use to evaluate your reconstruction algorithm. The final goal is to develop the algorithm so that it gets as close as possible to recovering the example object shown above from the noisy data. Pay attention to the size of the images, since this is not always the same, and could be different in in the final task.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "N = 1000; sigma = 1\n",
    "true_angles, images_stack = simulate_images(im, sigma, N)\n",
    "print(images_stack.shape)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Algorithm part 1: generate initial reference\n",
    "The projection matching algorithm works iteratively between estimating parameters $Y$ and reconstructing the density $A$. We need some starting point for the algorithm, i.e. we require some low-resolution prior knowledge of how the density $A$ looks. This can be tricky in practice, as often we will of course not know, and there are many approaches to this. For example, you could start with something like a sphere and assume almost nothing about the object you're trying to reconstuct, but this will not always converge to a solution. You could also start from something that already looks a lot like what you expect your object to look like (such as a similar protein), but this can introduce bias in the reconstruction.\n",
    "\n",
    "Here we give you a few options to select a reference. You can either choose to start with the same letter that is also used to generate the data, another letter or a solid circle. We also give you the choice to low-pass filter your reference. This is often done to remove some of the high-frequency components of the image to reduce the bias in the reconstruction.\n",
    "\n",
    "For the code you need to write, the provided functions should suffice. Additionally, you may have to specify variables, create an array or list to store values and use mathematical operations. Take a look at the [Python cheatsheet notebook](Python_cheatsheet.ipynb) if you are unfamiliar with Python syntax.\n",
    "\n",
    "    TO DO:\n",
    "    1. Select a reference to use (mind the size of the image)\n",
    "    2. Optionally, pre-process the reference using the low-pass filter\n",
    "    3. Show the reference and one of the input images from the simulated stack"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Useful functions\n",
    "def low_pass_filter_image(im, cutoff=5, apix=1):\n",
    "    \"\"\"\n",
    "    Returns a low-pass filter image from a tanh filter.\n",
    "    \"\"\"\n",
    "    \n",
    "    im_freq     = calculate_fourier_frequencies(im, apix=apix)\n",
    "    im_filter   = tanh_filter(im_freq, cutoff);\n",
    "    im_fft      = np.fft.rfftn(im)\n",
    "    im_fft_filtered = im_fft * im_filter\n",
    "    im_filtered = np.fft.irfftn(im_fft_filtered)\n",
    "    return im_filtered\n",
    "\n",
    "def make_circle(imsize, radius):\n",
    "    im = np.zeros((imsize, imsize))\n",
    "    for i in range(imsize):\n",
    "        for j in range(imsize):\n",
    "            if (i-imsize/2)**2 + (j-imsize/2)**2 < radius**2:\n",
    "                im[i,j] = 1\n",
    "    return im\n",
    "\n",
    "### to plot an image, use:\n",
    "#  imshow(im)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# code\n",
    "\n",
    "# for now, the reference is just the same letter as the object. \n",
    "# This is not the best way to do it, but it is a good start. You will need to change this. Use (some of) the provided functions to make a better reference.\n",
    "reference = make_letter(\"Q\") # pay attention to the size of the image!\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Algorithm part 2: Estimate the rotation angle $\\theta$\n",
    "Now that we have a reference to align the images to, we need to figure out the rotation angle $\\theta$ for each image. A simple way to do this is to rotate either the image or the reference by a small angle and then compute the 'similarity' between the two images. The rotation angle that produces the best similarity score counts as the optimal rotation angle. We generally choose to rotate the reference instead of all the images, since we only need to rotate one reference, as opposed to thousands of images. There are several ways of computing the similarity, or distance, between images. We could use the squared difference between observed data $X_i$ and the rotated reference image $R^{\\theta_i}A_{ref}$ for each pixel 1 to $N$:\n",
    "\n",
    "$$SQD = \\sum_{i=1}^N{\\Vert X_i - R^{\\theta_i} A_{ref} \\Vert}^2 $$\n",
    "$$= \\sum_{i=1}^N{\\Vert X_i \\Vert^2} + \\sum_{i=1}^N{ \\Vert R^{\\theta_i} A_{ref} \\Vert^2} - 2\\sum_{i=1}^N{X_i R^{\\theta_i} A_{ref}}$$\n",
    "\n",
    "Note that the first two terms are not dependend on simiarity of $X$ and $A$, while the third term is. The metric behaves similar to the cross-correlation between $X_i$ and $A_{ref}$, just inverted and with another scaling:\n",
    "\n",
    "$$ CC =\\frac{\\sum_{i=1}^N{X_i R^{\\theta_i} A_{ref}} }{N} $$\n",
    "\n",
    "    TO DO:\n",
    "    1. Generate a stack of rotated reference images with a small incremental rotation angle\n",
    "    2. Loop over all images, compare the image to the rotated reference stack by computing the distance score (SQD, correlation or your own idea)\n",
    "    3. Save the best alignment score and the corresponding rotation angle for each image\n",
    "    4. Make a plot of the alignment score vs. the angle for one single image for visualisation. Answer the question below about this plot.\n",
    "    5. Make a histogram of the best alignment angle for each image. The true rotation angles are uniformly distributed. Show if this is also what you estimate.\n",
    "    6. Make a plot of the true angle vs. the estimated alignment angle. Try running your code over (new) simulated data with with larger or smaller $\\sigma$ to see if your algorithm can still handle very noisy data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# useful functions\n",
    "def rotate_image(image, angle):\n",
    "    return ndimage.rotate(image, angle, reshape=False)\n",
    "\n",
    "def SQD(image1, image2):\n",
    "    return ((image1-image2)**2).sum()\n",
    "\n",
    "def corr(image1, image2):\n",
    "    return (image1*image2).mean()\n",
    "\n",
    "# helpful plotting functions (you can also use this as a reference to make the plots yourself. See also https://matplotlib.org/stable/plot_types/index.html)\n",
    "def plot_data(data, xlabel, ylabel, title):\n",
    "    _, ax = plt.subplots()\n",
    "    p = ax.plot(data[0], data[1])\n",
    "    ax.set(xlabel, ylabel, title)\n",
    "    ax.grid()\n",
    "    return p\n",
    "\n",
    "def plot_histogram(data, bins, xlabel, ylabel, title):\n",
    "    _, ax = plt.subplots()\n",
    "    hist = ax.hist(data, bins=bins)\n",
    "    ax.set(xlabel, ylabel, title)\n",
    "    ax.grid()\n",
    "    return hist\n",
    "\n",
    "def plot_scatter(data, xlabel, ylabel, title):\n",
    "    _, ax = plt.subplots()\n",
    "    scatter = ax.scatter(data[0], data[1])\n",
    "    ax.set(xlabel, ylabel, title)\n",
    "    ax.grid()\n",
    "    return scatter\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# code\n",
    "\n",
    "delta_angle = 5\n",
    "angles = np.arange(0, 360, delta_angle)\n",
    "Num_angles = len(angles)\n",
    "rotated_reference_stack = np.zeros((Num_angles, *reference.shape))\n",
    "alignment_result = []\n",
    "## generate rotated images\n",
    "\n",
    "## loop over images\n",
    "for i_num, im in enumerate(images_stack):\n",
    "    \n",
    "    ## compute similarity score between image and rotated reference\n",
    "    scores = np.zeros(Num_angles)\n",
    "    for ref_num, ref in enumerate(rotated_reference_stack):\n",
    "        score =\n",
    "        scores[ref_num] = score\n",
    "\n",
    "    ## find best match\n",
    "    best_score, best_angle = \n",
    "\n",
    "    ## store score and angle\n",
    "    alignment_result.append([best_score, best_angle])\n",
    "\n",
    "    if i_num == 0:\n",
    "        ## save the alignment scores for the first image, to use in the plot for point 4 on the To Do list above\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## make plot of alignment scores for the first image, as described in point 4 on the To Do list above\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## make histogram of the alignment angles (see point 5 on the To Do list above)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## make scatter plot of the alignment angles vs. the true angles (see point 6 on the To Do list above)\n",
    "# hint, the true angles were stored in the variable true_angles which we made when we simulated the images\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "## <font color='Teal'>Question 1:\n",
    "\n",
    "Answer the following questions about the plot you made of the alignment score vs. angle (To Do list point 4):\\\n",
    "A) what do you judge (by eye) the correct alignment angle to be?\\\n",
    "B) How many peaks or valleys can you see, and why would this be the case?\\\n",
    "C) What happens if the images were much noisier, say around $\\sigma=10$?\n",
    "\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Your answer here\n",
    "(double click this cell to edit)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Algorithm part 3: Reconstruct the object\n",
    "Finally, it is time to reconstruct the object from the images. In order to do this, we must rotate each image back by the rotation angle that we determined in the previous code block. \n",
    "\n",
    "    TO DO:\n",
    "    1. Rotate all images in the stack (\"image_stack\", created above) back by their determined rotation angle and sum them up\n",
    "    2. Show the reconstructed image"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# code\n",
    "im_reconstruct = np.zeros(reference.shape)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Test your algorithm on unknown data\n",
    "If you are satisified that your algorithm will work robustly on image data of varying quality (play around with the number of images, the $\\sigma$ value, the filtering options etc.), the real test will be to reconstruct one of the provided data sets below. These data sets are not easy to reconstruct, and you will not get a perfect result. If it seems that reconstructing the images is too difficult, try using a different letter as a reference, or use the circle, or try different values of 'delta_angle' (this increases the time to run the code).\n",
    "\n",
    "If you can tell us what object(s) are in the images, we are happy for now. If you are motivated, we will see how we can improve the reconstruction of more difficult data sets in the bonus exercises. (These are entirely optional, but of course recommended).\n",
    "\n",
    "You can load the data set of your choice below and either use the space available to copy- and paste your entire algorithm into one cell (recommended), or just re-run the cells above and look at the output. You can also make new cells if you need them, or create a single function that does the entire algorithm at once. For this part you will have to repeat the same 3 steps as before, create a suitable reference (this may take some trial and error, since you do not know the true structure), estimate the rotation angles and sum the aligned images to show a final reconstruction. Beware the images in these test data sets are not necessarily the same size as before, so adjust to code to handle this.\"\n",
    "\n",
    "(PS: remember that we are only giving you the images, not the true angles. So the plot you made in the second step of the algorithm where the estimated angles are plotted against the true ones will not be accurate and you do not have to show it)\n",
    "\n",
    "    TO DO:\n",
    "    1. Show the reconstruction from at least one of the three test data sets"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# data sets\n",
    "\n",
    "my_choice = 1\n",
    "\n",
    "images_stack = np.load(f\"data/data_set_{my_choice}.npy\")\n",
    "images_stack = images_stack.astype(np.float32)\n",
    "print(images_stack.shape) # beware the size of the image matches the size of the reference image"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# space to copy the code from the previous exercise or define a new function to reconstruct the image\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Bonus exercises\n",
    "If you are done with testing your algorithm on the provided data sets, you may have noticed that they are all a bit trickier to reconstruct then the training data. Each of them suffer from defects that we also encouter in experimental images. If you are confident in your coding abilities and would like to earn a few extra points, you can try your hand at upgrading your algorithm in one of the following ways to deal with more difficult data. Be aware that some of these upgrades can significantly increase the computational cost of the algorithm and may take a long time to run. Don't go too over board with the settings. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Bonus exercise 1: Dealing with translation\n",
    "So far, we have used the model that:\n",
    "<a id='equation_5'></a>\n",
    "$\n",
    "\\begin{equation}\n",
    "X_i = R^{\\theta_i} A + \\sigma G_i \\tag 5\n",
    "\\end{equation}\n",
    "$\n",
    "Which assumes that the only transformation applied to the images is a rotation. In practice particle images are 'picked' from an entire cryo-EM micrograph which contains hundreds of particles, and a square box containing the particle is extracted. The particle is not necessarily perfectly centered in this box with respect to the reference. Using this assumption extends equation 1 into:\n",
    "<a id='equation_6'></a>\n",
    "$\n",
    "\\begin{equation}\n",
    "X_i = T^{\\theta_i, t_i} A + \\sigma G_i \\tag 6\n",
    "\\end{equation}\n",
    "$\n",
    "Where the operator $T$ now includes both a rotation $\\theta_i$ and translation $t_i$ component. This means we now need to not only estimate the rotation angle but also the (x- and y-) translation for each image. If you would like to work on this bonus exercise, use notebook [hri_practical02_bonus1](hri_practical02_bonus1.ipynb)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Bonus exercise 2: Confidence and likelihood\n",
    "In the current algorithm we consider the estimated rotation angle to be the angle for which the image and rotated reference have the best match. You may have noticed, especially for higher levels of noise, that there often are several rotation angles that give an almost equal match to the rotated reference. The solution we find is called the least-squares solution, but it is not the only possibility. Using a little bit of Bayesian theory we can change the algorithm to a maximum likelihood estimation, which allows us much more control over the way we obtain the reconstruction. The crux of ML is that we are looking for a solution $A_{reconstruct}$ that maximizes $P(A_{reconstruct} | X_i, \\theta_i)$, the probability of our reconstruction being correct given all the images and their rotation angles. Using Bayes' law, we can expand this as:\n",
    "<a id='equation_7'></a>\n",
    "$\n",
    "\\begin{equation}\n",
    "P(A_{reconstruct} | X_i, \\theta_i) = \\frac{P(X_i|A_{reconstruct}, \\theta_i)P(A_{reconstruct}, \\theta_i)}{P(X_i)} \\tag 7\n",
    "\\end{equation}\n",
    "$\n",
    "If you would like to extend your algorithm to use Bayesian theory, use notebook [hri_practical02_bonus2](hri_practical02_bonus2.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Bonus exercise 3: Making the algorithm iterative\n",
    "The current algorithm aligns the images to a reference, then produces a reconstruction. We could of course use that reconstruction as a new reference to align all images to again to, hopefully, improve the alignment and therefore get a better reconstruction. And then we could do that again and again. This looks a lot more like what we actually would do with real data. If you do this it does become even more important to select a good initial reference, since any mistakes in the initial alignment could end up getting amplified through the iterative process. There are also the important questions about how quickly the result converges and when to stop iterating. If you would like to work on this bonus exercise, use notebook [hri_practical02_bonus3](hri_practical02_bonus3.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Bonus exercise 4: 2D classification of images\n",
    "Sometimes, we not only get images in our data of particles that we would like to reconstruct but also a lot of random junk (or even just images of pure noise). There are many ways to deal with this problem, but perhaps the most common is to try and classify the images and then filter out classes that are clearly bad particles. Classification is a common problem in machine learning and can be done using many different algorithms, either supervised or unsupervised. If you are interested in working on a classification problem as a bonus exercise, use notebook [hri_practical02_bonus4](hri_practical02_bonus4.ipynb) "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  },
  "vscode": {
   "interpreter": {
    "hash": "6339fb7d6b5790b70c356b6b3f2583ce36f2ae1fff9d286f274a2877217e9957"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
