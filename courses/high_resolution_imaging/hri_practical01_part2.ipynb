{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " <font color='DarkBlue'>High-Resolution Imaging (NB4020) \n",
    "----\n",
    "## <font color='CornflowerBlue'>Practical 1: Introduction to Fourier Optics and Image Processing\n",
    "\n",
    "## Fourier Series\n",
    "\n",
    "In this part, we shall see that $any$ 1-dimensional curve, can be split into a sum of simple sine waves. This seems counter-intuitive since many signals that we see clearly are not sine waves, and sometimes are even discontinous. The first person to carefully probe this problem was Joseph Baptiste Fourier (1768-1830), while developing a theory for the diffusion of heat (heat conduction). \n",
    "    \n",
    "He developed a partial differential equation — the heat equation — which describes how the temperature in a conductor changes over time. The temperature is a function of both space and time. Fourier began by assuming that the independent variables in the temperature can be separated: A solution for this heat equation could be easily derived when he assumed boundary conditions that are simple, e.g. the space dependence can be described by a sine wave. But in reality, this is of course rarely the case. For example, it may change linearly from hot at one end to cold at the other. Fourier claimed that any temperature distribution (and it turns out any function in general) could be expressed as an infinite sum of sine and cosine waves, now called a Fourier Series in honour of its inventor. \n",
    "    \n",
    "This gave rise to a powerful new concept in mathematics known as __Fourier Analysis__. \n",
    "\n",
    "### Fourier series and the box function\n",
    "    \n",
    "To begin understanding this analysis technique, let us now consider a type of discontinuous function called the box function. A box function $b(x)$ has the following property: \n",
    "    \n",
    "\n",
    "<a id='equation_2'></a>\n",
    "$\n",
    "\\begin{equation}\n",
    "    b(x) = \\begin{cases}\n",
    "    1 & -a/2 \\lt x \\lt a/2 \\\\\n",
    "    0 & everywhere \\\\\n",
    "    \\end{cases}  \\tag 2\\\n",
    "\\end{equation}\n",
    "$\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Play with this interactive plot to get an intuitive feel for the box function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from shared.misc import *\n",
    "play_with_box_wave();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we learnt [before](hri_practical01_part1#equation_1), a sinusoidal wave requires three parameters: Amplitude: $A$, Frequency: $f$, Phase: $\\phi$.  \n",
    "\n",
    "Thus, if we were to break up this functions into several sinusoidal waves, we need three parameters for each of these waves. They can be found using the Fourier Series. \n",
    "\n",
    "In it's most general form, a Fourier Series can be represented in the following way\n",
    "\n",
    "<a id='equation_3'></a>\n",
    "$\n",
    "\\begin{equation}\n",
    "      b(x) = A_0 + \\sum\\limits_{k=1}^F A_k \\cdot cos(\\frac{2 \\pi k \\cdot x}{P} - \\phi_k) \\tag 3\\ \n",
    "\\end{equation}\n",
    "$\n",
    "\n",
    "where, \n",
    "* $F$ = Maximum frequency up to which analysis is done \n",
    "* $P$ = length of input signal, $b(x)$ \n",
    "* $A_k$ = Amplitude of the wave at frequency $k$ \n",
    "* $\\phi_k$ = Phase of the wave at frequency $k$ \n",
    "\n",
    "\n",
    "\n",
    "If the input signal is periodic, then $P$ is its period. In case the input signal is not periodic, then the length of the signal itself becomes periodic. $A_0$ is the constant term, or it can be considered as the amplitude of the wave having zero frequency. \n",
    "\n",
    "The question now is, how do we find amplitudes and phases at each frequency?\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For each frequency component $k$, the amplitude and phase can be found out in the following way: \n",
    "\n",
    "<a id='equation_4'></a>\n",
    "$\n",
    "\\begin{equation}\n",
    "    a_k = \\frac{1}{P} \\sum\\limits_{i=0}^P b(i) \\cdot cos(\\frac{2\\pi k \\cdot i}{P}) \\tag 4\\\n",
    "\\end{equation}\n",
    "$\n",
    "\n",
    "<a id='equation_5'></a>\n",
    "$\n",
    "\\begin{equation}\n",
    "    b_k = \\frac{1}{P} \\sum\\limits_{i=0}^P b(i) \\cdot sin(\\frac{2\\pi k \\cdot i}{P}) \\tag 5\\\n",
    "  \\end{equation}\n",
    "$ \n",
    "\n",
    "<a id='equation_6'></a>\n",
    "$\n",
    "\\begin{equation}    \n",
    "    A_k = \\sqrt((a_k^2 + b_k^2)) \\tag 6\\\n",
    "\\end{equation}\n",
    "$\n",
    "\n",
    "<a id='equation_7'></a>\n",
    "$\n",
    "\\begin{equation} \n",
    "    \\phi_k = tan^{-1}(\\frac{b_k}{a_k})  \\tag 7\\\n",
    "\\end{equation}\n",
    "$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we increase the number of frequencies analysed, the reconstructed function more closely resemble the input step function $b(x)$.  \n",
    "\n",
    "Check for yourself with the following interactive plot! You can use the slider to change the total number of waves added ($F$ in the [equation 3](#equation_3)). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "play_with_reconstruction(getbox(500));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you choose only a single wave to reconstruct, what you end up is a constant line. This is sometimes referred to as the \"DC component\" of the signal, because in analysis of electric signals the constant term is the DC current. Increasing the number of added waves, you see the reconstructed wave approaching the box funcction better and better. Have a close look at the edges! "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "## <font color='Teal'>Question 1:\n",
    "\n",
    "Change the width of the boxfuntion in the above function ```play_with_reconstruction(getbox(500));```, i.e. change the number in brackets, and look at the effect on the DC component.  \n",
    "\n",
    "* Explain what you if you make the box wider or narrower. Can you make sense of this? Explain.\n",
    "\n",
    "__Hint__: the pixel width of the plot is 1000 pixels. \n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Your answer here\n",
    "(double click this cell to edit)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the above above we only looked at the total wave obtained from Fourier analysis. Let us have a closer look at the component waves. \n",
    "\n",
    "Run the next interactive widget to see the component sine waves for yourself. At index 0, you see the original box function, and at index 1 you see the reconstruction using the set of individual sine waves behind it. You have to click the \"Run interact\" button to display the plot.\n",
    "\n",
    "Change the number of waves and look at the amplitudes and phases for each of them. Note that at some frequencies, you see just a straight line, meaning the amplitude is very close to zero. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "get_ipython().magic('matplotlib notebook')\n",
    "check_first_few_waves(getbox(300));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To see if this theory works for any kind of signal, let us generate some random squiggly lines. Run the following command until you find yourself a random line you are happy with. Then run the code below to see the reconstruction (again you an play with the number of added waves in the reconstruction). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "get_ipython().magic('matplotlib inline')\n",
    "randomlines = plot_random_lines();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "play_with_reconstruction(randomlines);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "get_ipython().magic('matplotlib notebook')\n",
    "check_first_few_waves(randomlines);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By now we should have convinced you that signals can be broken down into constituent waves. This opens up a whole new way of analysing signals. \n",
    "\n",
    "The above plot also reveals that looking at all the constituent waves can be messy. You do already know that instead of looking at the wave itself, we can also describe it by a frequency, phase and amplitude. It is much easier to analyse the above plot by just looking at the amplitudes of invidiual waves associated with a particular frequency. Such a graph which plots the amplitudes and phases of different frequencies are known as frequency spectra. \n",
    "\n",
    "In the next section, let us look at some simple examples. \n",
    "\n",
    "[Prev: Introduction](hri_practical01_part1.ipynb)    \n",
    "[Next: Frequency Spectrum](hri_practical01_part3.ipynb)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.7.11 ('MD')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.11"
  },
  "vscode": {
   "interpreter": {
    "hash": "4b84bbe0ef3eb9a9935eefc88be2a4e7bf82e7903c59e692e7defe5cb9fd7e95"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
