{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font color='DarkBlue'>High-Resolution Imaging (NB4020) \n",
    "----\n",
    "## <font color='CornflowerBlue'>Practical 1: Introduction to Fourier Optics and Image Processing\n",
    "    \n",
    "## The contrast transfer function\n",
    " "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### What is contrast? \n",
    "\n",
    "Contrast is the property of an image which allows us to distinguish the signal from the background. In a TEM experiment, the signal information is transferred by the elastically scattered electrons which interfere at the detector. The interference between the scattered electrons and the unscattered electrons (the background) is what gives rise to the contrast in the image.\n",
    "\n",
    "To get an intuitive feeling for how contrast affects the image, let us first break it into small steps. First, we will understand the what contrast does to an image. Next, we will step into the fourier domain and see the effects of contrast at different spatial frequency. Finally, we will study the contrast transfer function and its effect on the image.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from shared.misc import *\n",
    "image = get_image('cameraman',256)\n",
    "change_contrast_slider(image);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, contrast is modified uniformly. The following equation describes how the image contrast is modified: \n",
    "$\n",
    "\\begin{equation}\n",
    "    I_{new} = I_{old} \\cdot c  \\tag{13}\n",
    "\\end{equation}\n",
    "$\n",
    "where\n",
    "- $c$ : contrast adjustment parameter\n",
    "- $I_{old}$ : original image\n",
    "- $I_{new}$ : contrast adjusted image"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the next section, we shall consider more complex contrast modifications that get us closer to understanding a true cryo-em image. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Frequency dependency of contrast\n",
    "As we saw above, a higher contrast (either positive or negative) is able to transfer information better. If contrast is set to zero, then no information is transferred i.e. the signal is not distinguishable from the background. In the example above, a zero contrast image would be a completely plain. In a real EM experiment we would just see background noise.\n",
    "\n",
    "If the contrast loss had been just like the above situation, then we would not have to worry. Correction for such images could be done by adjusting the contrast during post processing. However, in a cryo-EM image the contrast loss is more complicated. To begin with, we have to consider the effect of contrast loss at different spatial frequencies. In the interact below, you have three sliders using which you can change the contrast of the image at low, medium and high frequencies. In this simple example, we assume that the relationship between contrast and frequency is linear. In the left most figure, you see a 1 dimensional representation of contrast as a function of frequency. The middle figure shows the 2D grid highlighting how contrast is distributed over all 2-D spatial frequencies. The right most figure shows the effect of such a contrast distribution has on the input image. \n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from shared.misc import *\n",
    "play_with_frequency_contrast_slider();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you may have guessed, this type of contrast distribution is just another filter. It allows certain frequencies to pass through, while restricting other frequencies to varying degree. \n",
    "\n",
    "Mathematically, we can express the operation as follows\n",
    "<a id='equation_12'></a>\n",
    "$\n",
    "\\begin{equation}\n",
    "    \\mathscr{F}[I_{out}](k) = \\mathscr{F}[I_{true}](k) \\cdot CTF(k)\\tag {14}\\\n",
    "\\end{equation}\n",
    "$\n",
    "\n",
    "where: \n",
    "- $\\mathscr{F}[I_{out}](k)$: Represents the fourier transform of the output image\n",
    "- $\\mathscr{F}[I_{true}](k)$: Represents the fourier transform of the true undistorted image\n",
    "- $CTF(K)$: Represents the CTF distribution over the spatial frequencies $k$\n",
    "\n",
    "Now let us understand how the CTF distribution looks like for a real experimental image\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Contrast transfer function\n",
    "The frequency dependency of contrast in a TEM is described by the contrast transfer function (CTF). The CTF is due to the interference of scattered and non-scattered beams. This results in a periodic variation in the information transfer as a function of frequency. Thus, the CTF is described by a sinusoidal wave. \n",
    "\n",
    "The CTF is affected by the aberrations present in the instrument. The two important source of aberrations are spherical aberration and defocus. The following equation describes the CTF as a function of aberrations, and wavelength. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='equation_15'></a>\n",
    "$\n",
    "\\begin{equation}\n",
    "    CTF(k) = -sin[\\Delta\\phi + \\cfrac{-\\pi}{2}C_s\\lambda^3k^4 + \\pi\\lambda\\Delta_fk^2], \\tag {15}\\\n",
    "\\end{equation}\n",
    "$\n",
    "\n",
    "where\n",
    "\n",
    "$k$  - spatial frequency, $Cs$ - spherical aberration, $\\lambda$ - electron wavelength (defined by acceleration voltage), $\\Delta_f$ - defocus and $\\Delta\\phi$ - initial phase shift "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The electron wavelength here refers to the debroglie wavelength (after taking into account relativistic effects). The following relationship expresses the relativistic wavelength (in $\\AA$) as a function of accelerating voltage (in kV)\n",
    "$\n",
    "\\begin{equation}\n",
    "    \\lambda = \\frac{12.264}{\\sqrt{V \\cdot (1 + V \\cdot 0.98 \\cdot 10^{-6})}} \\tag {16}\\\n",
    "\\end{equation}\n",
    "$\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from shared.misc import *\n",
    "plot_ctf_simple();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In phase contrast imaging as we depend on for imaging biological samples in cryo-EM, contrast in the image is generated by defocusing the electron beam. We typically collect images in underfocus in order to image with a convergent beam; in this case the defocus is negative.\n",
    "\n",
    "Defocusing introduces frequency dependent phase reversals described by the contrast transfer function (CTF) above. As you can see from [equation 15](#equation_15) this modulation depends on a number of instrument parameters. Let us explore how these affect the shape of the transfer function with the following interactive plot."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### CTF modulations\n",
    "The complexity of a real experiment doesn't stop there. The sinusoidal CTF curve plotted above assumes a perfect instrument where scattered waves in all frequencies interefere with the same strength. However, this is not the case. The waves which get diffracted at higher angle becomes progressively weaker, due to inelastic scattering. The CTF therefore becomes damped oscillating wave. We model this dampening of the CTF with an envelope function which has an exponentially decaying intensities. This envelope function is parameterised by a term called the \"B-factor\". The following interactive shows the effect of such dampening. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_ctf_with_bfactor();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "## <font color='Teal'>Question 4:\n",
    "\n",
    "Play with the individual parameters to get a feeling for how they affect the oscillations of the CTF. \n",
    "\n",
    "* What are the parameters that most strongly affect the oscillations in the CTF? Rationalise also on the basis of [equation 15](#equation_15).         \n",
    "* Why can't we take images at perfect focus using a cryo-EM microscope?\n",
    "---\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Your answer here\n",
    "(double click this cell to edit)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let us have a look what effect the CTF would affect on a real image. Below we have imaged the TU Delft flame with the electron microscope. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from shared.misc import *\n",
    "plot_ctf_img_interactive();\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, a lot of the information is lost while gathering projection images. With such a poor quality data, it is not possible to perform 3-D reconstruction. So, how do we correct for it? You shall build a CTF correction algorithm yourself for your assignment! \n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## CTF correction - assignment\n",
    "The basic idea behind CTF correction is based on [equation 15](#equation_15). Given a corrupted image, we estimate the CTF and then try to retrieve the underlying true distribution. \n",
    "\n",
    "$\n",
    "\\begin{equation}\n",
    "    \\mathscr{F}[I_{ctf-corrected}](k) = \\frac{\\mathscr{F}[I_{out}](k)}{CTF_{estimated}(k)} \\tag {17}\\\n",
    "\\end{equation}\n",
    "$\n",
    "<a id=\"equation_18\"></a>\n",
    "$ \n",
    "\\begin{equation}\n",
    "    I_{ctf-corrected}(x) = \\mathscr{F^{-1}}{\\frac{\\mathscr{F}[I_{out}](k)}{CTF_{estimated}(k)}} \\tag {18}\\\n",
    "\\end{equation}\n",
    "$\n",
    "\n",
    "where: \n",
    "- $I_{ctf-corrected}(x)$: CTF-corrected image \n",
    "- $CTF_{estimated}(k)$: estimated CTF\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Think! \n",
    "What maybe the problem with using this formula directly? Hint: Think about the denominator."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We shall now try to implement some common strategies to perform CTF correction. First, we need to obtain a micrograph (image) of a sample. Run the following cells to see the micrograph of a spike protein."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# helper functions\n",
    "def calculate_fourier_transform(image):\n",
    "    return np.fft.fftshift(np.fft.fft2(image))\n",
    "\n",
    "def calculate_inverse_fourier_transform(ft):\n",
    "    return np.fft.ifft2(np.fft.ifftshift(ft))\n",
    "\n",
    "def plot_image_and_fourier_transform(image):\n",
    "    # get the mean and standard deviation of the image \n",
    "    mean, std = np.mean(image), np.std(image)\n",
    "    vmin = mean - 2*std\n",
    "    vmax = mean + 2*std\n",
    "    plt.figure(figsize=(12,4))\n",
    "    plt.subplot(1,2,1)\n",
    "    plt.imshow(image, cmap='gray')\n",
    "    plt.title('Image')\n",
    "    plt.axis('off')\n",
    "    plt.subplot(1,2,2)\n",
    "    ft = np.log(np.abs(calculate_fourier_transform(image)))\n",
    "    # get the mean and standard deviation of the ft\n",
    "    mean, std = np.mean(ft), np.std(ft)\n",
    "    vmin = np.percentile(ft, 5)\n",
    "    vmax = np.percentile(ft, 99.99)\n",
    "    plt.imshow(ft, cmap='gray', vmin=vmin, vmax=vmax)\n",
    "    plt.title('Fourier Transform')\n",
    "    plt.axis('off')\n",
    "    plt.show()\n",
    "\n",
    "def simulate_CTF_curve(estimated_defocus,  estimated_delta_phi, estimate_b_factor):\n",
    "    voltage = 300e3 # given\n",
    "    Cs = 0 # given\n",
    "    max_freq=0.5 # given\n",
    "    num_points = 256//2 # given\n",
    "    k = np.linspace(0, max_freq, num_points)\n",
    "    estimated_defocus_in_angstrom = estimated_defocus * 1e4\n",
    "    wavelength = relativistic_lambda(voltage)\n",
    "    gamma = (-np.pi/2)*Cs*np.power(wavelength,3)*np.power(k,4)+np.pi*wavelength*estimated_defocus_in_angstrom*np.power(k,2) \n",
    "    CTF = -1*np.sin(estimated_delta_phi+gamma) \n",
    "    if estimate_b_factor != 0:\n",
    "        CTF *= np.exp(-estimate_b_factor*k**2)\n",
    "\n",
    "    return k, CTF\n",
    "\n",
    "def convert_1d_ctf_to_2d_ctf(ctf_1d):\n",
    "    from scipy import interpolate\n",
    "    n = len(ctf_1d)\n",
    "    apix = 1\n",
    "    freq = np.linspace(1/(n*apix), 1/(2*apix), n, endpoint=True)\n",
    "    assert len(freq) == len(ctf_1d), \"Frequency and CTF must have the same size\"\n",
    "    ctf_interpolate = interpolate.interp1d(freq, ctf_1d, fill_value=\"extrapolate\")\n",
    "\n",
    "    freq_x, freq_y = np.meshgrid(np.linspace(-1/(2*apix), 1/(2*apix), 2*n, endpoint=True), np.linspace(-1/(2*apix), 1/(2*apix), 2*n, endpoint=True))\n",
    "    # generate a 2d CTF grid\n",
    "    freq_2d = np.sqrt(freq_x**2 + freq_y**2)\n",
    "    # mask freq_2d \n",
    "    circular_mask = (freq_2d <= 1).astype(np.int)\n",
    "    freq_2d = freq_2d * circular_mask\n",
    "    ctf_2d = ctf_interpolate(freq_2d)\n",
    "    # apply the CTF to the image      \n",
    "    return ctf_2d\n",
    "\n",
    "def plot_input_image_and_corrected_image(input_image, corrected_image):\n",
    "    fig, ax = plt.subplots(1,2, figsize=(12,4))\n",
    "    input_image = (input_image - input_image.min()) / (input_image.max() - input_image.min())\n",
    "    ax[0].imshow(input_image, cmap='gray')\n",
    "    ax[0].set_title('Input Image')\n",
    "    ax[0].axis('off')\n",
    "    corrected_image = (corrected_image - corrected_image.min()) / (corrected_image.max() - corrected_image.min())\n",
    "    ax[1].imshow(corrected_image, cmap='gray')\n",
    "    ax[1].set_title('Corrected Image')\n",
    "    ax[1].axis('off')\n",
    "    plt.show()\n",
    "\n",
    "def plot_radial_amplitudes(images, labels):\n",
    "    for image, label in zip(images, labels):\n",
    "        rp = compute_radial_profile(image)\n",
    "        freq = frequency_array(rp, apix=1)\n",
    "        plt.plot(freq, np.log(rp), label=label)\n",
    "    \n",
    "    plt.xlabel(r\"Spatial Frequency ($\\AA^{-1}$)\")\n",
    "    plt.ylabel(\"Radially Averaged Amplitude\")\n",
    "    plt.xlim(-0.01,0.21)\n",
    "    plt.ylim(5, 20)\n",
    "    plt.legend()\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from shared.misc import *\n",
    "micrograph = get_image(\"spike_protein\", 256)\n",
    "plt.imshow(micrograph,cmap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Step 1: Calculate the fourier transform of the image and plot. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ft_micrograph = calculate_fourier_transform(micrograph)\n",
    "plot_image_and_fourier_transform(micrograph)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Step 2: Estimate the CTF parameters.\n",
    "\n",
    "Use the sliders provided to estimate the CTF parameters. Turn on the \"flip\" switch and see what happens to the sign of the CTF. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_defocus_and_1D_CTF_interactive(ft_micrograph);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Step 3: Use [equation 18](#equation_18) to calculate the CTF corrected image\n",
    "\n",
    "Note: the following functions are useful. \n",
    "1) simulate_CTF_curve() \n",
    "2) convert_1d_ctf_to_2d_ctf()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# # your solution here\n",
    "# bfactor_estimated = # fill in\n",
    "# defocus_estimated =  # fill in\n",
    "# delta_phi_estimated =  # fill in\n",
    "# k, CTF_estimated_1D = # fill in\n",
    "# CTF_estimated_2D =  #"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now use the estimated CTF to get the fourier transform of the true undistorted image\n",
    "\n",
    "Let us explore three methods to use the estimated CTF to correct the image.\n",
    "\n",
    "### Method 1: Phase Flipping\n",
    "In this method, we use the estimated CTF to flip the amplitudes of the frequencies corresponding to negative contrast. \n",
    "\n",
    "Step 1: Find the indices corresponding to negative contrast. \n",
    "\n",
    "Step 2: Find the indices corresponding to positive contrast (inverse of the above)\n",
    "\n",
    "Step 3: Multiply the fourier amplitudes corresponding to negative contrast with -1 (Equivalent to shifting the phases by $\\pi$). Fourier amplitudes of the positive contrast frequencies are the same as original image.\n",
    "\n",
    "Step 4: Take the inverse fourier transform to get the CTF corrected image.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# # YOUR SOLUTION HERE\n",
    "\n",
    "# ft_ctf_corrected_image_method_1 = #\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, take the inverse fourier transform to obtain the corrected image. Use the helped function to calculate the inverse transform. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# find the corrected image\n",
    "ctf_corrected_image_method_1 = calculate_inverse_fourier_transform(ft_ctf_corrected_image_method_1)\n",
    "\n",
    "# get the absolute value of the corrected image\n",
    "ctf_corrected_image_method_1 = np.abs(ctf_corrected_image_method_1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot the input image and the corrected image\n",
    "plot_input_image_and_corrected_image(micrograph, ctf_corrected_image_method_1)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Think!\n",
    "Do you see any reduction in the ringing artifacts? "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Method 2: Full CTF Correction with zero crossing\n",
    "\n",
    "In the first method, we only estimated the CTF to identify the negative contrast frequencies. However, we can use the CTF to correct the fourier amplitudes of all the frequencies using [equation 18](#equation_18). Here, for each frequency we divide the fourier amplitude by the CTF value at that frequency. However, since certain frequencies correspond to very low contrast we should take care to avoid division by zero. One method to avoid this is to ignore the frequencies where the CTF is near zero. We can do this by setting a threshold. The amplitudes of those frequencies which have a CTF value below the threshold are set to zero.\n",
    "\n",
    "Step 1: Set a threshold value for the CTF.\n",
    "\n",
    "Step 2: Find the indices corresponding to non zero CTF values (i.e. CTF > threshold)\n",
    "\n",
    "Step 3: Divide the fourier amplitudes with the CTF values at the corresponding frequencies.\n",
    "\n",
    "Step 4: Set the fourier amplitudes corresponding to frequencies with CTF < threshold to zero.\n",
    "\n",
    "Step 5: Take the inverse fourier transform to get the CTF corrected image.\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# # your solution here\n",
    "# min_threshold = # fill in\n",
    "# non_zero_indices = # fill in\n",
    "# ft_ctf_corrected_image_method_2 = # fill in\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# find the corrected image\n",
    "ctf_corrected_image_method_2 = calculate_inverse_fourier_transform(ft_ctf_corrected_image_method_2)\n",
    "\n",
    "# get the absolute value of the corrected image\n",
    "ctf_corrected_image_method_2 = np.abs(ctf_corrected_image_method_2)\n",
    "\n",
    "# plot the input image and the corrected image\n",
    "plot_input_image_and_corrected_image(micrograph, ctf_corrected_image_method_2)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Method 3: Full CTF correction using a Wiener filter\n",
    "In this method, we also correct the fourier amplitudes for the full frequency range. However, here we also take into account the signal to noise ratio of the image. This is done by using a wiener filter. We choose a constant value for the wiener factor. The wiener factor is added to the denominator of [equation 18](#equation_18). This ensures that the amplitudes of the frequencies with low CTF values are not amplified too much.\n",
    "\n",
    "$\n",
    "\\begin{equation}\n",
    "    \\mathscr{F}[I_{ctf-corrected}](k) = \\frac{\\mathscr{F}[I_{out}](k) \\cdot CTF_{estimated}(k)}{CTF_{estimated}(k)^2 + SNR} \\tag {19}\\\n",
    "\\end{equation}\n",
    "$\n",
    "<a id='equation_20'></a>\n",
    "\\begin{equation}\n",
    "    I_{ctf-corrected}(x) = \\mathscr{F^{-1}}\\left\\{\\mathscr{F}[I_{ctf-corrected}](k)\\right\\} \\tag {20}\\\n",
    "\\end{equation}\n",
    "$\n",
    "\n",
    "Step 1: Estimate the signal to noise ration $SNR$\n",
    "\n",
    "Step 2: Use [equation 20](#equation_20) to calculate the CTF corrected image\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# # your solution here\n",
    "# SNR = # fill in\n",
    "# ft_ctf_corrected_image_method_3 = # fill in\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# find the corrected image\n",
    "ctf_corrected_image_method_3 = calculate_inverse_fourier_transform(ft_ctf_corrected_image_method_3)\n",
    "\n",
    "# get the absolute value of the corrected image\n",
    "ctf_corrected_image_method_3 = np.abs(ctf_corrected_image_method_3)\n",
    "\n",
    "# plot the input image and the corrected image\n",
    "plot_input_image_and_corrected_image(micrograph, ctf_corrected_image_method_3)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compare\n",
    "\n",
    "Now, let us compare the output from the three methods. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the input image and the corrected image using the three methods\n",
    "fig, ax = plt.subplots(1,4, figsize=(16,4))\n",
    "ax[0].imshow(micrograph, cmap='gray')\n",
    "ax[0].set_title('Input Image')\n",
    "ax[1].imshow(ctf_corrected_image_method_1, cmap='gray')\n",
    "ax[1].set_title('Corrected: Phase Flip')\n",
    "ax[2].imshow(ctf_corrected_image_method_2, cmap='gray')\n",
    "ax[2].set_title('Corrected: Full CTF (using threshold)')\n",
    "ax[3].imshow(ctf_corrected_image_method_3, cmap='gray')\n",
    "ax[3].set_title('Corrected: Wiener Filter')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now also look at the effect of the CTF correction on the 1D power spectrum."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## <font color='Teal'>Question 5:\n",
    "* How does output from phase flipping compare to the other two methods? What do we gain by using the full CTF correction methods?\n",
    "* What happens if you vary the threshold in method 2? \n",
    "* What happens if you vary the SNR in method 3? What is the optimal value for the SNR?\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Your answer here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Think!\n",
    "Can we also understand the effect of different CTF correction methods in the fourier domain? To do this, we can compute the 1-D power spectrum of the image. A 1-D power spectrum is obtained by averaging the 2-D power spectrum along the radial direction (i.e. along the frequency). Run the following cell and note how different CTF correction methods affect the 1-D power spectrum.\n",
    "\n",
    "* What do the dips in the 1-D power spectrum correspond to?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_radial_amplitudes(\n",
    "            images=[micrograph, ctf_corrected_image_method_1, ctf_corrected_image_method_2, ctf_corrected_image_method_3], \\\n",
    "            labels=['Input Image', 'Phase Flip', 'Full CTF: Threshold', 'Wiener Filter'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From all the exercises in this practical, you should now have gained a solid basis to move on and understand the principles behind reconstructing structures from EM experiments as we will do in the next two practicals.\n",
    "\n",
    "Launch the virtual learning environment for starting the SPA practical.  \n",
    "\n",
    "[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.tudelft.nl%2Faj-lab%2Fteaching.git/master)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Prev: Convolutions](hri_practical01_part6.ipynb)    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.10.4 ('teaching')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  },
  "vscode": {
   "interpreter": {
    "hash": "6339fb7d6b5790b70c356b6b3f2583ce36f2ae1fff9d286f274a2877217e9957"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
